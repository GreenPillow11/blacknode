package types

import (
	"errors"
)

// Valid check ClaimItem , return an error if it is not valid
func (m *ClaimItem) Valid() error {
	if m.From == "" {
		return errors.New("from is empty")
	}
	if m.To == "" {
		return errors.New("to is empty")
	}
	if m.Signature == "" {
		return errors.New("signature is empty")
	}
	if m.TxID.IsEmpty() {
		return errors.New("txID is empty")
	}
	if m.Message == "" {
		return errors.New("message is empty")
	}
	if m.Signer == "" {
		return errors.New("signer is empty")
	}
	return nil
}

// NewBlockClaims create a new item of BlockClaims
func NewBlockClaims(height int64) *BlockClaims {
	return &BlockClaims{
		Height: height,
		Claims: make([]ClaimItem, 0),
	}
}

// IsEmpty to determinate whether there are txitm in this TxOut
func (m *BlockClaims) IsEmpty() bool {
	return len(m.Claims) == 0
}

// Valid check every item , return an error if even one of it is not valid
func (m *BlockClaims) Valid() error {
	for _, c := range m.Claims {
		if err := c.Valid(); err != nil {
			return err
		}
	}
	return nil
}
