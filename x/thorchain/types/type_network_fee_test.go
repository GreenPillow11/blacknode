package types

import (
	. "gopkg.in/check.v1"

	"gitlab.com/blackprotocol/blacknode/common"
)

type NetworkFeeSuite struct{}

var _ = Suite(&NetworkFeeSuite{})

func (NetworkFeeSuite) TestNetworkFee(c *C) {
	n := NewNetworkFee(common.BTCChain, 1, bnbSingleTxFee.Uint64())
	c.Check(n.Valid(), IsNil)
	n1 := NewNetworkFee(common.EmptyChain, 1, bnbSingleTxFee.Uint64())
	c.Check(n1.Valid(), NotNil)
	n2 := NewNetworkFee(common.BTCChain, 0, bnbSingleTxFee.Uint64())
	c.Check(n2.Valid(), NotNil)

	n3 := NewNetworkFee(common.BTCChain, 1, 0)
	c.Check(n3.Valid(), NotNil)
}
