package types

import (
	"fmt"

	"github.com/blang/semver"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

// QueryResLastBlockHeights used to return the block height query
type QueryResLastBlockHeights struct {
	Chain            common.Chain `json:"chain"`
	LastChainHeight  int64        `json:"last_observed_in"`
	LastSignedHeight int64        `json:"last_signed_out"`
	Thorchain        int64        `json:"thorchain"`
}

// String implement fmt.Stringer return a string representation of QueryResLastBlockHeights
func (h QueryResLastBlockHeights) String() string {
	return fmt.Sprintf("Chain: %d, Signed: %d, THORChain: %d", h.LastChainHeight, h.LastSignedHeight, h.Thorchain)
}

// QueryKeygenBlock query keygen, displays signed keygen requests
type QueryKeygenBlock struct {
	KeygenBlock KeygenBlock `json:"keygen_block"`
	Signature   string      `json:"signature"`
}

// String implement fmt.Stringer
func (n QueryKeygenBlock) String() string {
	return n.KeygenBlock.String()
}

type QueryVersion struct {
	Current semver.Version `json:"current"`
	Next    semver.Version `json:"next"`
	Querier semver.Version `json:"querier"`
}

// NewQueryNodeAccount create a new QueryNodeAccount based on the given node account parameter
func NewQueryNodeAccount(na NodeAccount) *QueryNodeAccount {
	return &QueryNodeAccount{
		NodeAddress:         na.NodeAddress.String(),
		Status:              na.Status,
		PubKeySet:           &na.PubKeySet,
		ValidatorConsPubKey: na.ValidatorConsPubKey,
		Bond:                na.Bond,
		ActiveBlockHeight:   na.ActiveBlockHeight,
		BondAddress:         na.BondAddress,
		StatusSince:         na.StatusSince,
		SignerMembership:    na.GetSignerMembership().Strings(),
		RequestedToLeave:    na.RequestedToLeave,
		ForcedToLeave:       na.ForcedToLeave,
		LeaveScore:          na.LeaveScore,
		IPAddress:           na.IPAddress,
		Version:             na.GetVersion().String(),
	}
}

// NewQueryTxOutItem create a new QueryTxOutItem based on the given txout item parameter
func NewQueryTxOutItem(toi TxOutItem, height int64) *QueryTxOutItem {
	return &QueryTxOutItem{
		Chain:                 toi.Chain,
		ToAddress:             toi.ToAddress,
		VaultPubKey:           toi.VaultPubKey,
		Coin:                  toi.Coin,
		Memo:                  toi.Memo,
		MaxGas:                toi.MaxGas,
		GasRate:               toi.GasRate,
		InHash:                toi.InHash,
		OutHash:               toi.OutHash,
		ModuleName:            toi.ModuleName,
		Aggregator:            toi.Aggregator,
		AggregatorTargetAsset: toi.AggregatorTargetAsset,
		AggregatorTargetLimit: toi.AggregatorTargetLimit,
		Height:                height,
	}
}

// QuerySaver holds all the information related to a saver
type QuerySaver struct {
	Asset              common.Asset   `json:"asset"`
	AssetAddress       common.Address `json:"asset_address"`
	LastAddHeight      int64          `json:"last_add_height,omitempty"`
	LastWithdrawHeight int64          `json:"last_withdraw_height,omitempty"`
	Units              cosmos.Uint    `json:"units"`
	AssetDepositValue  cosmos.Uint    `json:"asset_deposit_value"`
}

// NewQuerySaver creates a new QuerySaver based on the given liquidity provider parameters
func NewQuerySaver(lp LiquidityProvider) QuerySaver {
	return QuerySaver{
		Asset:              lp.Asset.GetLayer1Asset(),
		AssetAddress:       lp.AssetAddress,
		LastAddHeight:      lp.LastAddHeight,
		LastWithdrawHeight: lp.LastWithdrawHeight,
		Units:              lp.Units,
		AssetDepositValue:  lp.AssetDepositValue,
	}
}

// QueryVaultsPubKeys represent the result for query vaults pubkeys
type QueryVaultsPubKeys struct {
	Asgard    []QueryVaultPubKeyContract `json:"asgard"`
	Yggdrasil []QueryVaultPubKeyContract `json:"yggdrasil"`
}
