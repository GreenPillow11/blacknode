package types

import (
	"errors"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

var _ cosmos.Msg = &MsgClaim{}

// NewMsgClaim create new instance of MsgClaim
func NewMsgClaim(from, to, signature string, txID common.TxID, message string, signer cosmos.AccAddress) *MsgClaim {
	return &MsgClaim{
		From:      from,
		To:        to,
		Signature: signature,
		TxID:      txID,
		Message:   message,
		Signer:    signer.String(),
	}
}

func (m *MsgClaim) Route() string { return RouterKey }
func (m *MsgClaim) Type() string  { return "claim" }
func (m *MsgClaim) ValidateBasic() error {
	if m.From == "" {
		return errors.New("from is empty")
	}
	if m.To == "" {
		return errors.New("to is empty")
	}
	if m.Signature == "" {
		return errors.New("signature is empty")
	}
	if m.TxID.IsEmpty() {
		return errors.New("txID is empty")
	}
	if m.Message == "" {
		return errors.New("message is empty")
	}
	if m.Signer == "" {
		return errors.New("signer is empty")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (m *MsgClaim) GetSignBytes() []byte {
	return cosmos.MustSortJSON(ModuleCdc.MustMarshalJSON(m))
}

// GetSigners defines whose signature is required
func (m *MsgClaim) GetSigners() []cosmos.AccAddress {
	signerAddr, _ := cosmos.AccAddressFromBech32(m.Signer)
	return []cosmos.AccAddress{signerAddr}
}
