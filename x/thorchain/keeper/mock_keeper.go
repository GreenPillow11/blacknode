//nolint:forcetypeassert
package keeper

import (
	"github.com/blang/semver"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/stretchr/testify/mock"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	kvTypes "gitlab.com/blackprotocol/blacknode/x/thorchain/keeper/types"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

// MockKeeper is a dummy mock of keeper
type MockKeeper struct {
	mock.Mock
}

func (m *MockKeeper) Cdc() codec.BinaryCodec {
	args := m.Called()
	return args.Get(0).(codec.BinaryCodec)
}

func (m *MockKeeper) GetVersion() semver.Version {
	args := m.Called()
	return args.Get(0).(semver.Version)
}

func (m *MockKeeper) GetVersionWithCtx(ctx cosmos.Context) (semver.Version, bool) {
	args := m.Called(ctx)
	return args.Get(0).(semver.Version), args.Bool(1)
}

func (m *MockKeeper) SetVersionWithCtx(ctx cosmos.Context, v semver.Version) {
	_ = m.Called(ctx, v)
}

func (m *MockKeeper) GetKey(ctx cosmos.Context, prefix kvTypes.DbPrefix, key string) string {
	args := m.Called(ctx, prefix, key)
	return args.String(0)
}

func (m *MockKeeper) GetStoreVersion(ctx cosmos.Context) int64 {
	args := m.Called(ctx)
	return int64(args.Int(0))
}

func (m *MockKeeper) SetStoreVersion(ctx cosmos.Context, ver int64) {
	_ = m.Called(ctx, ver)
}

func (m *MockKeeper) GetRuneBalanceOfModule(ctx cosmos.Context, moduleName string) cosmos.Uint {
	args := m.Called(ctx, moduleName)
	return args.Get(0).(cosmos.Uint)
}

func (m *MockKeeper) GetBalanceOfModule(ctx cosmos.Context, moduleName, denom string) cosmos.Uint {
	args := m.Called(ctx, moduleName, denom)
	return args.Get(1).(cosmos.Uint)
}

func (m *MockKeeper) SendFromModuleToModule(ctx cosmos.Context, from, to string, coin common.Coins) error {
	args := m.Called(ctx, from, to, coin)
	return args.Error(0)
}

func (m *MockKeeper) SendFromAccountToModule(ctx cosmos.Context, from cosmos.AccAddress, to string, coin common.Coins) error {
	args := m.Called(ctx, from, to, coin)
	return args.Error(0)
}

func (m *MockKeeper) SendFromModuleToAccount(ctx cosmos.Context, from string, to cosmos.AccAddress, coin common.Coins) error {
	args := m.Called(ctx, from, to, coin)
	return args.Error(0)
}

func (m *MockKeeper) MintToModule(ctx cosmos.Context, module string, coin common.Coin) error {
	args := m.Called(ctx, module, coin)
	return args.Error(0)
}

func (m *MockKeeper) BurnFromModule(ctx cosmos.Context, module string, coin common.Coin) error {
	args := m.Called(ctx, module, coin)
	return args.Error(0)
}

func (m *MockKeeper) MintAndSendToAccount(ctx cosmos.Context, to cosmos.AccAddress, coin common.Coin) error {
	args := m.Called(ctx, to, coin)
	return args.Error(0)
}

func (m *MockKeeper) GetModuleAddress(module string) (common.Address, error) {
	args := m.Called(module)
	return args.Get(0).(common.Address), args.Error(1)
}

func (m *MockKeeper) GetModuleAccAddress(module string) cosmos.AccAddress {
	args := m.Called(module)
	return args.Get(0).(cosmos.AccAddress)
}

func (m *MockKeeper) GetBalance(ctx cosmos.Context, addr cosmos.AccAddress) cosmos.Coins {
	args := m.Called(ctx, addr)
	return args.Get(0).(cosmos.Coins)
}

func (m *MockKeeper) HasCoins(ctx cosmos.Context, addr cosmos.AccAddress, coins cosmos.Coins) bool {
	args := m.Called(ctx, addr, coins)
	return args.Bool(0)
}

func (m *MockKeeper) GetAccount(ctx cosmos.Context, addr cosmos.AccAddress) cosmos.Account {
	args := m.Called(ctx, addr)
	return args.Get(0).(cosmos.Account)
}

// passthrough funcs
func (m *MockKeeper) SendCoins(ctx cosmos.Context, from, to cosmos.AccAddress, coins cosmos.Coins) error {
	args := m.Called(ctx, from, to, coins)
	return args.Error(0)
}

func (m *MockKeeper) AddCoins(ctx cosmos.Context, addr cosmos.AccAddress, coins cosmos.Coins) error {
	args := m.Called(ctx, addr, coins)
	return args.Error(0)
}

func (m *MockKeeper) GetPoolIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetPool(ctx cosmos.Context, asset common.Asset) (Pool, error) {
	args := m.Called(ctx, asset)
	return args.Get(0).(Pool), args.Error(1)
}

func (m *MockKeeper) GetPools(ctx cosmos.Context) (Pools, error) {
	args := m.Called(ctx)
	return args.Get(0).(Pools), args.Error(1)
}

func (m *MockKeeper) SetPool(ctx cosmos.Context, pool Pool) error {
	args := m.Called(ctx, pool)
	return args.Error(0)
}

func (m *MockKeeper) PoolExist(ctx cosmos.Context, asset common.Asset) bool {
	args := m.Called(ctx, asset)
	return args.Bool(0)
}

func (m *MockKeeper) RemovePool(ctx cosmos.Context, asset common.Asset) {
	_ = m.Called(ctx, asset)
}

func (m *MockKeeper) SetPoolLUVI(ctx cosmos.Context, asset common.Asset, luvi cosmos.Uint) {
	_ = m.Called(ctx, asset, luvi)
}

func (m *MockKeeper) GetPoolLUVI(ctx cosmos.Context, asset common.Asset) (cosmos.Uint, error) {
	args := m.Called(ctx, asset)
	return args.Get(0).(cosmos.Uint), args.Error(1)
}

func (m *MockKeeper) SetLastSignedHeight(ctx cosmos.Context, height int64) error {
	args := m.Called(ctx, height)
	return args.Error(0)
}

func (m *MockKeeper) GetLastSignedHeight(ctx cosmos.Context) (int64, error) {
	args := m.Called(ctx)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetLastChainHeight(ctx cosmos.Context, chain common.Chain, height int64) error {
	args := m.Called(ctx, chain, height)
	return args.Error(0)
}

func (m *MockKeeper) GetLastChainHeight(ctx cosmos.Context, chain common.Chain) (int64, error) {
	args := m.Called(ctx, chain)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) GetLastChainHeights(ctx cosmos.Context) (map[common.Chain]int64, error) {
	args := m.Called(ctx)
	return args.Get(0).(map[common.Chain]int64), args.Error(1)
}

func (m *MockKeeper) SetLastObserveHeight(ctx cosmos.Context, chain common.Chain, address cosmos.AccAddress, height int64) error {
	args := m.Called(ctx, chain, address, height)
	return args.Error(0)
}

func (m *MockKeeper) GetLastObserveHeight(ctx cosmos.Context, address cosmos.AccAddress) (map[common.Chain]int64, error) {
	args := m.Called(ctx, address)
	return args.Get(0).(map[common.Chain]int64), args.Error(1)
}

func (m *MockKeeper) GetLiquidityProvider(ctx cosmos.Context, asset common.Asset, addr common.Address) (LiquidityProvider, error) {
	args := m.Called(ctx, asset, addr)
	return args.Get(0).(LiquidityProvider), args.Error(1)
}

func (m *MockKeeper) GetLiquidityProviderIterator(ctx cosmos.Context, asset common.Asset) cosmos.Iterator {
	args := m.Called(ctx, asset)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) SetLiquidityProvider(ctx cosmos.Context, lp LiquidityProvider) {
	args := m.Called(ctx, lp)
	_ = args
}

func (m *MockKeeper) RemoveLiquidityProvider(ctx cosmos.Context, lp LiquidityProvider) {
	args := m.Called(ctx, lp)
	_ = args
}

func (m *MockKeeper) GetTotalSupply(ctx cosmos.Context, asset common.Asset) cosmos.Uint {
	args := m.Called(ctx, asset)
	return args.Get(0).(cosmos.Uint)
}

func (m *MockKeeper) TotalActiveValidators(ctx cosmos.Context) (int, error) {
	args := m.Called(ctx)
	return args.Int(0), args.Error(1)
}

func (m *MockKeeper) ListValidatorsWithBond(ctx cosmos.Context) (NodeAccounts, error) {
	args := m.Called(ctx)
	return args.Get(0).(NodeAccounts), args.Error(1)
}

func (m *MockKeeper) ListValidatorsByStatus(ctx cosmos.Context, status NodeStatus) (NodeAccounts, error) {
	args := m.Called(ctx, status)
	return args.Get(0).(NodeAccounts), args.Error(1)
}

func (m *MockKeeper) ListActiveValidators(ctx cosmos.Context) (NodeAccounts, error) {
	args := m.Called(ctx)
	return args.Get(0).(NodeAccounts), args.Error(1)
}

func (m *MockKeeper) GetLowestActiveVersion(ctx cosmos.Context) semver.Version {
	args := m.Called(ctx)
	return args.Get(0).(semver.Version)
}

func (m *MockKeeper) GetMinJoinVersion(ctx cosmos.Context) semver.Version {
	args := m.Called(ctx)
	return args.Get(0).(semver.Version)
}

func (m *MockKeeper) GetNodeAccount(ctx cosmos.Context, addr cosmos.AccAddress) (NodeAccount, error) {
	args := m.Called(ctx, addr)
	return args.Get(0).(NodeAccount), args.Error(1)
}

func (m *MockKeeper) GetNodeAccountByPubKey(ctx cosmos.Context, pk common.PubKey) (NodeAccount, error) {
	args := m.Called(ctx, pk)
	return args.Get(0).(NodeAccount), args.Error(1)
}

func (m *MockKeeper) SetNodeAccount(ctx cosmos.Context, na NodeAccount) error {
	args := m.Called(ctx, na)
	return args.Error(0)
}

func (m *MockKeeper) EnsureNodeKeysUnique(ctx cosmos.Context, consensusPubKey string, pubKeys common.PubKeySet) error {
	args := m.Called(ctx, consensusPubKey, pubKeys)
	return args.Error(0)
}

func (m *MockKeeper) GetNodeAccountIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetNodeAccountSlashPoints(ctx cosmos.Context, addr cosmos.AccAddress) (int64, error) {
	args := m.Called(ctx, addr)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetNodeAccountSlashPoints(ctx cosmos.Context, addr cosmos.AccAddress, slashPoints int64) {
	args := m.Called(ctx, addr, slashPoints)
	_ = args
}

func (m *MockKeeper) IncNodeAccountSlashPoints(ctx cosmos.Context, addr cosmos.AccAddress, slashPoints int64) error {
	args := m.Called(ctx, addr, slashPoints)
	return args.Error(0)
}

func (m *MockKeeper) DecNodeAccountSlashPoints(ctx cosmos.Context, addr cosmos.AccAddress, slashPoints int64) error {
	args := m.Called(ctx, addr, slashPoints)
	return args.Error(0)
}

func (m *MockKeeper) ResetNodeAccountSlashPoints(ctx cosmos.Context, addr cosmos.AccAddress) {
	args := m.Called(ctx, addr)
	_ = args
}

func (m *MockKeeper) GetNodeAccountJail(ctx cosmos.Context, addr cosmos.AccAddress) (Jail, error) {
	args := m.Called(ctx, addr)
	return args.Get(0).(Jail), args.Error(1)
}

func (m *MockKeeper) SetNodeAccountJail(ctx cosmos.Context, addr cosmos.AccAddress, height int64, reason string) error {
	args := m.Called(ctx, addr, height, reason)
	return args.Error(0)
}

func (m *MockKeeper) ReleaseNodeAccountFromJail(ctx cosmos.Context, addr cosmos.AccAddress) error {
	args := m.Called(ctx, addr)
	return args.Error(0)
}

func (m *MockKeeper) SetBondProviders(ctx cosmos.Context, bp BondProviders) error {
	args := m.Called(ctx, bp)
	return args.Error(0)
}

func (m *MockKeeper) GetBondProviders(ctx cosmos.Context, addr cosmos.AccAddress) (BondProviders, error) {
	args := m.Called(ctx, addr)
	return args.Get(0).(BondProviders), args.Error(1)
}

func (m *MockKeeper) GetObservingAddresses(ctx cosmos.Context) ([]cosmos.AccAddress, error) {
	args := m.Called(ctx)
	return args.Get(0).([]cosmos.AccAddress), args.Error(1)
}

func (m *MockKeeper) AddObservingAddresses(ctx cosmos.Context, inAddresses []cosmos.AccAddress) error {
	args := m.Called(ctx, inAddresses)
	return args.Error(0)
}

func (m *MockKeeper) ClearObservingAddresses(ctx cosmos.Context) {
	args := m.Called(ctx)
	_ = args
}

func (m *MockKeeper) SetObservedTxInVoter(ctx cosmos.Context, tx ObservedTxVoter) {
	args := m.Called(ctx, tx)
	_ = args
}

func (m *MockKeeper) GetObservedTxInVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetObservedTxInVoter(ctx cosmos.Context, hash common.TxID) (ObservedTxVoter, error) {
	args := m.Called(ctx, hash)
	return args.Get(0).(ObservedTxVoter), args.Error(1)
}

func (m *MockKeeper) SetObservedTxOutVoter(ctx cosmos.Context, tx ObservedTxVoter) {
	args := m.Called(ctx, tx)
	_ = args
}

func (m *MockKeeper) GetObservedTxOutVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetObservedTxOutVoter(ctx cosmos.Context, hash common.TxID) (ObservedTxVoter, error) {
	args := m.Called(ctx, hash)
	return args.Get(0).(ObservedTxVoter), args.Error(1)
}

func (m *MockKeeper) SetObservedLink(ctx cosmos.Context, tx1, tx2 common.TxID) {
	args := m.Called(ctx, tx1, tx2)
	_ = args
}

func (m *MockKeeper) GetObservedLink(ctx cosmos.Context, inhash common.TxID) []common.TxID {
	args := m.Called(ctx, inhash)
	return args.Get(0).([]common.TxID)
}

func (m *MockKeeper) SetTxOut(ctx cosmos.Context, blockOut *TxOut) error {
	args := m.Called(ctx, blockOut)
	return args.Error(0)
}

func (m *MockKeeper) AppendTxOut(ctx cosmos.Context, height int64, item TxOutItem) error {
	args := m.Called(ctx, height, item)
	return args.Error(0)
}

func (m *MockKeeper) ClearTxOut(ctx cosmos.Context, height int64) error {
	args := m.Called(ctx, height)
	return args.Error(0)
}

func (m *MockKeeper) GetTxOutIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetTxOut(ctx cosmos.Context, height int64) (*TxOut, error) {
	args := m.Called(ctx, height)
	return args.Get(0).(*TxOut), args.Error(1)
}

func (m *MockKeeper) GetTxOutValue(ctx cosmos.Context, height int64) (cosmos.Uint, error) {
	args := m.Called(ctx, height)
	return args.Get(0).(cosmos.Uint), args.Error(1)
}

func (m *MockKeeper) AddToLiquidityFees(ctx cosmos.Context, asset common.Asset, fee cosmos.Uint) error {
	args := m.Called(ctx, asset, fee)
	return args.Error(0)
}

func (m *MockKeeper) GetTotalLiquidityFees(ctx cosmos.Context, height uint64) (cosmos.Uint, error) {
	args := m.Called(ctx, height)
	return args.Get(0).(cosmos.Uint), args.Error(1)
}

func (m *MockKeeper) GetPoolLiquidityFees(ctx cosmos.Context, height uint64, asset common.Asset) (cosmos.Uint, error) {
	args := m.Called(ctx, height, asset)
	return args.Get(0).(cosmos.Uint), args.Error(1)
}

func (m *MockKeeper) GetRollingPoolLiquidityFee(ctx cosmos.Context, asset common.Asset) (uint64, error) {
	args := m.Called(ctx, asset)
	return uint64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) ResetRollingPoolLiquidityFee(ctx cosmos.Context, asset common.Asset) {
	args := m.Called(ctx, asset)
	_ = args
}

func (m *MockKeeper) GetVaultIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) VaultExists(ctx cosmos.Context, pk common.PubKey) bool {
	args := m.Called(ctx, pk)
	return args.Bool(0)
}

func (m *MockKeeper) SetVault(ctx cosmos.Context, vault Vault) error {
	args := m.Called(ctx, vault)
	return args.Error(0)
}

func (m *MockKeeper) GetVault(ctx cosmos.Context, pk common.PubKey) (Vault, error) {
	args := m.Called(ctx, pk)
	return args.Get(0).(Vault), args.Error(1)
}

func (m *MockKeeper) HasValidVaultPools(ctx cosmos.Context) (bool, error) {
	args := m.Called(ctx)
	return args.Bool(0), args.Error(1)
}

func (m *MockKeeper) GetAsgardVaults(ctx cosmos.Context) (Vaults, error) {
	args := m.Called(ctx)
	return args.Get(0).(Vaults), args.Error(1)
}

func (m *MockKeeper) GetAsgardVaultsByStatus(ctx cosmos.Context, vs VaultStatus) (Vaults, error) {
	args := m.Called(ctx, vs)
	return args.Get(0).(Vaults), args.Error(1)
}

func (m *MockKeeper) GetLeastSecure(ctx cosmos.Context, vaults Vaults, signingTransPeriod int64) Vault {
	args := m.Called(ctx, vaults, signingTransPeriod)
	return args.Get(0).(Vault)
}

func (m *MockKeeper) GetMostSecure(ctx cosmos.Context, vaults Vaults, signingTransPeriod int64) Vault {
	args := m.Called(ctx, vaults, signingTransPeriod)
	return args.Get(0).(Vault)
}

func (m *MockKeeper) SortBySecurity(ctx cosmos.Context, vaults Vaults, signingTransPeriod int64) Vaults {
	args := m.Called(ctx, vaults, signingTransPeriod)
	return args.Get(0).(Vaults)
}

func (m *MockKeeper) DeleteVault(ctx cosmos.Context, pk common.PubKey) error {
	args := m.Called(ctx, pk)
	return args.Error(0)
}

func (m *MockKeeper) RemoveFromAsgardIndex(ctx cosmos.Context, pubkey common.PubKey) error {
	args := m.Called(ctx, pubkey)
	return args.Error(0)
}

func (m *MockKeeper) AddPoolFeeToReserve(ctx cosmos.Context, fee cosmos.Uint) error {
	args := m.Called(ctx, fee)
	return args.Error(0)
}

func (m *MockKeeper) AddBondFeeToReserve(ctx cosmos.Context, fee cosmos.Uint) error {
	args := m.Called(ctx, fee)
	return args.Error(0)
}

func (m *MockKeeper) GetNetwork(ctx cosmos.Context) (Network, error) {
	args := m.Called(ctx)
	return args.Get(0).(Network), args.Error(1)
}

func (m *MockKeeper) SetNetwork(ctx cosmos.Context, data Network) error {
	args := m.Called(ctx, data)
	return args.Error(0)
}

func (m *MockKeeper) GetPOL(ctx cosmos.Context) (ProtocolOwnedLiquidity, error) {
	args := m.Called(ctx)
	return args.Get(0).(ProtocolOwnedLiquidity), args.Error(1)
}

func (m *MockKeeper) SetPOL(ctx cosmos.Context, data ProtocolOwnedLiquidity) error {
	args := m.Called(ctx, data)
	return args.Error(0)
}

func (m *MockKeeper) SetTssVoter(ctx cosmos.Context, tss TssVoter) {
	args := m.Called(ctx, tss)
	_ = args
}

func (m *MockKeeper) GetTssVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetTssVoter(ctx cosmos.Context, hash string) (TssVoter, error) {
	args := m.Called(ctx, hash)
	return args.Get(0).(TssVoter), args.Error(1)
}

func (m *MockKeeper) SetTssKeygenMetric(ctx cosmos.Context, metric *TssKeygenMetric) {
	args := m.Called(ctx, metric)
	_ = args
}

func (m *MockKeeper) GetTssKeygenMetric(ctx cosmos.Context, key common.PubKey) (*TssKeygenMetric, error) {
	args := m.Called(ctx, key)
	return args.Get(0).(*TssKeygenMetric), args.Error(1)
}

func (m *MockKeeper) SetTssKeysignMetric(ctx cosmos.Context, metric *TssKeysignMetric) {
	args := m.Called(ctx, metric)
	_ = args
}

func (m *MockKeeper) GetTssKeysignMetric(ctx cosmos.Context, txID common.TxID) (*TssKeysignMetric, error) {
	args := m.Called(ctx, txID)
	return args.Get(0).(*TssKeysignMetric), args.Error(1)
}

func (m *MockKeeper) GetLatestTssKeysignMetric(ctx cosmos.Context) (*TssKeysignMetric, error) {
	args := m.Called(ctx)
	return args.Get(0).(*TssKeysignMetric), args.Error(1)
}

func (m *MockKeeper) SetTssKeysignFailVoter(ctx cosmos.Context, tss TssKeysignFailVoter) {
	args := m.Called(ctx, tss)
	_ = args
}

func (m *MockKeeper) GetTssKeysignFailVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetTssKeysignFailVoter(ctx cosmos.Context, id string) (TssKeysignFailVoter, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(TssKeysignFailVoter), args.Error(1)
}

func (m *MockKeeper) SetKeygenBlock(ctx cosmos.Context, keygenBlock KeygenBlock) {
	args := m.Called(ctx, keygenBlock)
	_ = args
}

func (m *MockKeeper) GetKeygenBlockIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetKeygenBlock(ctx cosmos.Context, height int64) (KeygenBlock, error) {
	args := m.Called(ctx, height)
	return args.Get(0).(KeygenBlock), args.Error(1)
}

func (m *MockKeeper) SetBanVoter(ctx cosmos.Context, bv BanVoter) {
	args := m.Called(ctx, bv)
	_ = args
}

func (m *MockKeeper) GetBanVoter(ctx cosmos.Context, addr cosmos.AccAddress) (BanVoter, error) {
	args := m.Called(ctx, addr)
	return args.Get(0).(BanVoter), args.Error(1)
}

func (m *MockKeeper) GetBanVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) RagnarokInProgress(ctx cosmos.Context) bool {
	args := m.Called(ctx)
	return args.Bool(0)
}

func (m *MockKeeper) GetRagnarokBlockHeight(ctx cosmos.Context) (int64, error) {
	args := m.Called(ctx)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetRagnarokBlockHeight(ctx cosmos.Context, height int64) {
	args := m.Called(ctx, height)
	_ = args
}

func (m *MockKeeper) GetRagnarokNth(ctx cosmos.Context) (int64, error) {
	args := m.Called(ctx)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetRagnarokNth(ctx cosmos.Context, n int64) {
	args := m.Called(ctx, n)
	_ = args
}

func (m *MockKeeper) GetRagnarokPending(ctx cosmos.Context) (int64, error) {
	args := m.Called(ctx)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetRagnarokPending(ctx cosmos.Context, n int64) {
	args := m.Called(ctx, n)
	_ = args
}

func (m *MockKeeper) GetRagnarokWithdrawPosition(ctx cosmos.Context) (RagnarokWithdrawPosition, error) {
	args := m.Called(ctx)
	return args.Get(0).(RagnarokWithdrawPosition), args.Error(1)
}

func (m *MockKeeper) SetRagnarokWithdrawPosition(ctx cosmos.Context, position RagnarokWithdrawPosition) {
	args := m.Called(ctx, position)
	_ = args
}

func (m *MockKeeper) SetPoolRagnarokStart(ctx cosmos.Context, asset common.Asset) {
	args := m.Called(ctx, asset)
	_ = args
}

func (m *MockKeeper) GetPoolRagnarokStart(ctx cosmos.Context, asset common.Asset) (int64, error) {
	args := m.Called(ctx, asset)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetErrataTxVoter(ctx cosmos.Context, etv ErrataTxVoter) {
	args := m.Called(ctx, etv)
	_ = args
}

func (m *MockKeeper) GetErrataTxVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetErrataTxVoter(ctx cosmos.Context, txID common.TxID, chain common.Chain) (ErrataTxVoter, error) {
	args := m.Called(ctx, txID, chain)
	return args.Get(0).(ErrataTxVoter), args.Error(1)
}

func (m *MockKeeper) SetSwapQueueItem(ctx cosmos.Context, msg MsgSwap, i int) error {
	args := m.Called(ctx, msg, i)
	return args.Error(0)
}

func (m *MockKeeper) GetSwapQueueIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetSwapQueueItem(ctx cosmos.Context, txID common.TxID, i int) (MsgSwap, error) {
	args := m.Called(ctx, txID, i)
	return args.Get(0).(MsgSwap), args.Error(1)
}

func (m *MockKeeper) HasSwapQueueItem(ctx cosmos.Context, txID common.TxID, i int) bool {
	args := m.Called(ctx, txID, i)
	return args.Bool(0)
}

func (m *MockKeeper) RemoveSwapQueueItem(ctx cosmos.Context, txID common.TxID, i int) {
	args := m.Called(ctx, txID, i)
	_ = args
}

func (m *MockKeeper) SetOrderBookItem(ctx cosmos.Context, msg MsgSwap) error {
	args := m.Called(ctx, msg)
	return args.Error(0)
}

func (m *MockKeeper) GetOrderBookItemIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetOrderBookItem(ctx cosmos.Context, txID common.TxID) (MsgSwap, error) {
	args := m.Called(ctx, txID)
	return args.Get(0).(MsgSwap), args.Error(1)
}

func (m *MockKeeper) HasOrderBookItem(ctx cosmos.Context, txID common.TxID) bool {
	args := m.Called(ctx, txID)
	return args.Bool(0)
}

func (m *MockKeeper) RemoveOrderBookItem(ctx cosmos.Context, txID common.TxID) error {
	args := m.Called(ctx, txID)
	return args.Error(0)
}

func (m *MockKeeper) GetOrderBookIndexIterator(ctx cosmos.Context, ot types.OrderType, from, to common.Asset) cosmos.Iterator {
	args := m.Called(ctx, ot, from, to)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) SetOrderBookIndex(ctx cosmos.Context, swap MsgSwap) error {
	args := m.Called(ctx, swap)
	return args.Error(0)
}

func (m *MockKeeper) GetOrderBookIndex(ctx cosmos.Context, swap MsgSwap) (common.TxIDs, error) {
	args := m.Called(ctx, swap)
	return args.Get(0).(common.TxIDs), args.Error(1)
}

func (m *MockKeeper) HasOrderBookIndex(ctx cosmos.Context, swap MsgSwap) (bool, error) {
	args := m.Called(ctx, swap)
	return args.Bool(0), args.Error(1)
}

func (m *MockKeeper) RemoveOrderBookIndex(ctx cosmos.Context, swap MsgSwap) error {
	args := m.Called(ctx, swap)
	return args.Error(0)
}

func (m *MockKeeper) SetOrderBookProcessor(ctx cosmos.Context, in []bool) error {
	args := m.Called(ctx, in)
	return args.Error(0)
}

func (m *MockKeeper) GetOrderBookProcessor(ctx cosmos.Context) ([]bool, error) {
	args := m.Called(ctx)
	return args.Get(0).([]bool), args.Error(1)
}

func (m *MockKeeper) GetMimir(ctx cosmos.Context, key string) (int64, error) {
	args := m.Called(ctx, key)
	return int64(args.Int(0)), args.Error(1)
}

func (m *MockKeeper) SetMimir(ctx cosmos.Context, key string, value int64) {
	args := m.Called(ctx, key, value)
	_ = args
}

func (m *MockKeeper) GetNodeMimirs(ctx cosmos.Context, key string) (NodeMimirs, error) {
	args := m.Called(ctx, key)
	return args.Get(0).(NodeMimirs), args.Error(1)
}

func (m *MockKeeper) SetNodeMimir(ctx cosmos.Context, key string, value int64, acc cosmos.AccAddress) error {
	args := m.Called(ctx, key, value, acc)
	return args.Error(0)
}

func (m *MockKeeper) GetMimirIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetNodeMimirIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) DeleteMimir(ctx cosmos.Context, key string) error {
	args := m.Called(ctx, key)
	return args.Error(0)
}

func (m *MockKeeper) GetNodePauseChain(ctx cosmos.Context, acc cosmos.AccAddress) int64 {
	args := m.Called(ctx, acc)
	return int64(args.Int(0))
}

func (m *MockKeeper) SetNodePauseChain(ctx cosmos.Context, acc cosmos.AccAddress) {
	args := m.Called(ctx, acc)
	_ = args
}

func (m *MockKeeper) GetNetworkFee(ctx cosmos.Context, chain common.Chain) (NetworkFee, error) {
	args := m.Called(ctx, chain)
	return args.Get(0).(NetworkFee), args.Error(1)
}

func (m *MockKeeper) SaveNetworkFee(ctx cosmos.Context, chain common.Chain, networkFee NetworkFee) error {
	args := m.Called(ctx, chain, networkFee)
	return args.Error(0)
}

func (m *MockKeeper) GetNetworkFeeIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) SetObservedNetworkFeeVoter(ctx cosmos.Context, networkFeeVoter ObservedNetworkFeeVoter) {
	args := m.Called(ctx, networkFeeVoter)
	_ = args
}

func (m *MockKeeper) GetObservedNetworkFeeVoterIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetObservedNetworkFeeVoter(ctx cosmos.Context, height int64, chain common.Chain, rate int64) (ObservedNetworkFeeVoter, error) {
	args := m.Called(ctx, height, chain, rate)
	return args.Get(0).(ObservedNetworkFeeVoter), args.Error(1)
}

func (m *MockKeeper) SetChainContract(ctx cosmos.Context, cc ChainContract) {
	args := m.Called(ctx, cc)
	_ = args
}

func (m *MockKeeper) GetChainContract(ctx cosmos.Context, _ common.Chain) (ChainContract, error) {
	args := m.Called(ctx)
	return args.Get(0).(ChainContract), args.Error(1)
}

func (m *MockKeeper) GetChainContracts(ctx cosmos.Context, chains common.Chains) []ChainContract {
	args := m.Called(ctx, chains)
	return args.Get(0).([]ChainContract)
}

func (m *MockKeeper) GetChainContractIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) SetSolvencyVoter(ctx cosmos.Context, _ SolvencyVoter) {
	args := m.Called(ctx)
	_ = args
}

func (m *MockKeeper) GetSolvencyVoter(ctx cosmos.Context, txID common.TxID, chain common.Chain) (SolvencyVoter, error) {
	args := m.Called(ctx, txID, chain)
	return args.Get(0).(SolvencyVoter), args.Error(1)
}

func (m *MockKeeper) THORNameExists(ctx cosmos.Context, name string) bool {
	args := m.Called(ctx, name)
	return args.Bool(0)
}

func (m *MockKeeper) GetTHORName(ctx cosmos.Context, name string) (THORName, error) {
	args := m.Called(ctx, name)
	return args.Get(0).(THORName), args.Error(1)
}

func (m *MockKeeper) SetTHORName(ctx cosmos.Context, name THORName) {
	args := m.Called(ctx, name)
	_ = args
}

func (m *MockKeeper) GetTHORNameIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) DeleteTHORName(ctx cosmos.Context, name string) error {
	args := m.Called(ctx, name)
	return args.Error(0)
}

func (m *MockKeeper) SetBlockClaims(ctx cosmos.Context, blockOut *types.BlockClaims) error {
	args := m.Called(ctx, blockOut)
	return args.Error(0)
}

func (m *MockKeeper) AppendClaimItem(ctx cosmos.Context, height int64, item types.ClaimItem) error {
	args := m.Called(ctx, height, item)
	return args.Error(0)
}

func (m *MockKeeper) ClearBlockClaims(ctx cosmos.Context, height int64) error {
	args := m.Called(ctx, height)
	return args.Error(0)
}

func (m *MockKeeper) GetBlockClaimsIterator(ctx cosmos.Context) cosmos.Iterator {
	args := m.Called(ctx)
	return args.Get(0).(cosmos.Iterator)
}

func (m *MockKeeper) GetBlockClaims(ctx cosmos.Context, height int64) (*types.BlockClaims, error) {
	args := m.Called(ctx, height)
	return args.Get(0).(*types.BlockClaims), args.Error(1)
}
