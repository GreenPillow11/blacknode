package keeperv1

import (
	"fmt"
	"strconv"

	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

func (k KVStore) setBlockClaims(ctx cosmos.Context, key string, record *BlockClaims) {
	store := ctx.KVStore(k.storeKey)
	buf := k.cdc.MustMarshal(record)
	if buf == nil {
		store.Delete([]byte(key))
	} else {
		store.Set([]byte(key), buf)
	}
}

func (k KVStore) getBlockClaims(ctx cosmos.Context, key string, record *BlockClaims) (bool, error) {
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(key)) {
		return false, nil
	}

	bz := store.Get([]byte(key))
	if err := k.cdc.Unmarshal(bz, record); err != nil {
		return true, dbError(ctx, fmt.Sprintf("Unmarshal kvstore: (%T) %s", record, key), err)
	}
	return true, nil
}

// AppendClaimItem - append the given claim item to BlockClaims
func (k KVStore) AppendClaimItem(ctx cosmos.Context, height int64, item ClaimItem) error {
	block, err := k.GetBlockClaims(ctx, height)
	if err != nil {
		return err
	}
	block.Claims = append(block.Claims, item)
	return k.SetBlockClaims(ctx, block)
}

// ClearBlockClaims - remove the BlockClaims of the given height from key value  store
func (k KVStore) ClearBlockClaims(ctx cosmos.Context, height int64) error {
	k.del(ctx, k.GetKey(ctx, prefixClaimBlock, strconv.FormatInt(height, 10)))
	return nil
}

// SetBlockClaims - write the given BlockClaims information to key value store
func (k KVStore) SetBlockClaims(ctx cosmos.Context, blockOut *BlockClaims) error {
	if blockOut == nil || blockOut.IsEmpty() {
		return nil
	}
	k.setBlockClaims(ctx, k.GetKey(ctx, prefixClaimBlock, strconv.FormatInt(blockOut.Height, 10)), blockOut)
	return nil
}

// GetBlockClaimsIterator iterate tx out
func (k KVStore) GetBlockClaimsIterator(ctx cosmos.Context) cosmos.Iterator {
	return k.getIterator(ctx, prefixClaimBlock)
}

// GetBlockClaims - write the given claim information to key value store
func (k KVStore) GetBlockClaims(ctx cosmos.Context, height int64) (*BlockClaims, error) {
	record := NewBlockClaims(height)
	_, err := k.getBlockClaims(ctx, k.GetKey(ctx, prefixClaimBlock, strconv.FormatInt(height, 10)), record)
	return record, err
}
