package thorchain

import (
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

func withdraw(ctx cosmos.Context, msg MsgWithdrawLiquidity, mgr Manager) (cosmos.Uint, cosmos.Uint, cosmos.Uint, cosmos.Uint, cosmos.Uint, error) {
	return withdrawV1(ctx, msg, mgr)
}
