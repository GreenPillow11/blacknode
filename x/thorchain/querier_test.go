package thorchain

import (
	"errors"
	"testing"

	"github.com/blang/semver"
	"github.com/cosmos/cosmos-sdk/crypto/keys/secp256k1"
	cryptotypes "github.com/cosmos/cosmos-sdk/crypto/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/keeper"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

type MockSigner struct {
	mock.Mock
}

//nolint:forcetypeassert
func (ms *MockSigner) Sign(uid string, msg []byte) ([]byte, cryptotypes.PubKey, error) {
	args := ms.Called(uid, msg)
	return args.Get(0).([]byte), args.Get(1).(cryptotypes.PubKey), args.Error(2)
}

//nolint:forcetypeassert
func (ms *MockSigner) SignByAddress(address sdk.Address, msg []byte) ([]byte, cryptotypes.PubKey, error) {
	args := ms.Called(address, msg)
	return args.Get(0).([]byte), args.Get(1).(cryptotypes.PubKey), args.Error(2)
}

func GetQuerierForTest() (cosmos.Context, *GRPCQuerier) {
	ctx, mgr := GetManagersForTest()
	return ctx, NewGRPCQuerier(mgr, &MockSigner{})
}

func TestQueryKeysign(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	ctx = ctx.WithBlockHeight(12)
	pk := GetRandomPubKey()
	toAddr := GetRandomBNBAddress()
	txOut := NewTxOut(1)
	txOutItem := types.TxOutItem{
		Chain:       common.BNBChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(100*common.One)),
	}
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()

	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	if !ok {
		t.FailNow()
	}
	mockSigner, ok := querier.kbs.(*MockSigner)
	if !ok {
		t.FailNow()
	}
	mockSignCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3}, pubKey, errors.New("fail to sign"))
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	// when block height is negative , it should return an error
	res, err := querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: -1})
	assert.NotNil(t, err)
	assert.Nil(t, res)
	// when block height is larger than current latest block height , it should return error
	res, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 100})
	assert.NotNil(t, err)
	assert.Nil(t, res)

	// When fail to get tx out , it should error
	call := mockKeeper.On("GetTxOut", ctx, int64(5)).Return(txOut, errKaboom)
	res, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	assert.NotNil(t, err)
	assert.Nil(t, res)
	call.Unset()
	mockKeeper.On("GetTxOut", mock.Anything, int64(5)).Return(txOut, nil)
	// fail to sign should return an error
	res, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	assert.NotNil(t, err)
	assert.Nil(t, res)
	mockSignCall.Unset()

	mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3}, pubKey, nil)
	res, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	assert.Nil(t, err)
	assert.NotNil(t, res)
}

func TestQueryPool(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when fail to parse asset , it should return an error
	resp, err := querier.Pool(ctx, &types.QueryPoolRequest{
		Asset: "whateverblablala.TTT123",
	})
	assert.NotNil(t, err)
	assert.Nil(t, resp)

	// when fail to get pool , it should return an error
	getPoolCall := mockKeeper.On("GetPool", mock.Anything, mock.Anything).Return(types.Pool{}, errors.New("fail to get pool"))
	resp, err = querier.Pool(ctx, &types.QueryPoolRequest{Asset: "BNB.BNB"})
	assert.NotNil(t, err)
	assert.Nil(t, resp)

	getPoolCall.Unset()
	getPoolCall = getPoolCall.On("GetPool", mock.Anything, mock.Anything).Return(types.Pool{}, nil)
	resp, err = querier.Pool(ctx, &types.QueryPoolRequest{Asset: "BNB.BNB"})
	assert.NotNil(t, err)
	assert.Nil(t, resp)
	getPoolCall.Unset()

	bnbPool := types.Pool{
		Asset:               common.BNBAsset,
		BalanceRune:         cosmos.NewUint(1000 * common.One),
		BalanceAsset:        cosmos.NewUint(1000 * common.One),
		LPUnits:             cosmos.NewUint(1000 * common.One),
		Status:              PoolAvailable,
		Decimals:            8,
		StatusSince:         1,
		PendingInboundAsset: cosmos.ZeroUint(),
		PendingInboundRune:  cosmos.ZeroUint(),
	}
	bnbSaver := types.Pool{
		Asset:               common.BNBAsset.GetSyntheticAsset(),
		BalanceRune:         cosmos.NewUint(100 * common.One),
		BalanceAsset:        cosmos.NewUint(100 * common.One),
		LPUnits:             cosmos.NewUint(100 * common.One),
		Status:              PoolAvailable,
		Decimals:            8,
		StatusSince:         1,
		PendingInboundAsset: cosmos.ZeroUint(),
		PendingInboundRune:  cosmos.ZeroUint(),
	}
	// fail to get saver pool should return an error
	getPoolCall = getPoolCall.
		On("GetPool", mock.Anything, common.BNBAsset).Return(bnbPool, nil).
		On("GetPool", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(types.Pool{}, errors.New("fail to get saver vault"))
	resp, err = querier.Pool(ctx, &types.QueryPoolRequest{Asset: "BNB.BNB"})
	assert.NotNil(t, err)
	assert.Nil(t, resp)
	getPoolCall.Unset()

	getPoolCall.
		On("GetPool", mock.Anything, common.BNBAsset).Return(bnbPool, nil).
		On("GetPool", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(bnbSaver, nil).
		On("GetTotalSupply", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(cosmos.NewUint(100*common.One)).
		On("GetMimir", mock.Anything, constants.MaxSynthPerPoolDepth.String()).Return(-1, nil)
	resp, err = querier.Pool(ctx, &types.QueryPoolRequest{Asset: "BNB.BNB"})
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.NotNil(t, resp.Pool)
}

func TestQueryPools(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	store := ctx.KVStore(keyThorchain)
	iter := cosmos.KVStorePrefixIterator(store, []byte("pool/"))
	poolCall := mockKeeper.On("GetPoolIterator", mock.Anything).Return(iter)
	// when fail to parse asset , it should return an error
	resp, err := querier.Pools(ctx, &types.QueryPoolsRequest{})
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	poolCall = poolCall.Unset()
	bnbPool := types.Pool{
		Asset:               common.BNBAsset,
		BalanceRune:         cosmos.NewUint(1000 * common.One),
		BalanceAsset:        cosmos.NewUint(1000 * common.One),
		LPUnits:             cosmos.NewUint(1000 * common.One),
		Status:              PoolAvailable,
		Decimals:            8,
		StatusSince:         1,
		PendingInboundAsset: cosmos.ZeroUint(),
		PendingInboundRune:  cosmos.ZeroUint(),
	}
	bnbSaver := types.Pool{
		Asset:               common.BNBAsset.GetSyntheticAsset(),
		BalanceRune:         cosmos.NewUint(100 * common.One),
		BalanceAsset:        cosmos.NewUint(100 * common.One),
		LPUnits:             cosmos.NewUint(100 * common.One),
		Status:              PoolAvailable,
		Decimals:            8,
		StatusSince:         1,
		PendingInboundAsset: cosmos.ZeroUint(),
		PendingInboundRune:  cosmos.ZeroUint(),
	}
	k := getInMemoryKeeper()
	require.Nil(t, k.SetPool(ctx, bnbPool))
	require.Nil(t, k.SetPool(ctx, bnbSaver))
	// when fail to get the saver vault, it should return an error

	getVaultCall := poolCall.On("GetPool", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(bnbSaver, errors.New("fail to get saver vault"))
	poolCall = poolCall.On("GetPoolIterator", mock.Anything).Return(k.GetPoolIterator(ctx)).
		On("Cdc").Return(k.Cdc()).
		On("GetPool", mock.Anything, common.BNBAsset).Return(bnbPool, nil).
		On("GetTotalSupply", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(cosmos.NewUint(100*common.One)).
		On("GetMimir", mock.Anything, constants.MaxSynthPerPoolDepth.String()).Return(-1, nil)
	resp, err = querier.Pools(ctx, &types.QueryPoolsRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)

	poolCall = poolCall.Unset()
	getVaultCall.Unset()

	getVaultCall = poolCall.On("GetPool", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(bnbSaver, nil)
	poolCall.On("GetPoolIterator", mock.Anything).Return(k.GetPoolIterator(ctx)).
		On("Cdc").Return(k.Cdc()).
		On("GetPool", mock.Anything, common.BNBAsset).Return(bnbPool, nil).
		On("GetTotalSupply", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(cosmos.NewUint(100*common.One)).
		On("GetMimir", mock.Anything, constants.MaxSynthPerPoolDepth.String()).Return(-1, nil)
	resp, err = querier.Pools(ctx, &types.QueryPoolsRequest{})
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.NotNil(t, resp.Pools)
	getVaultCall.Unset()
}

func TestInboundAddress(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	getVaultsCall := mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(types.Vaults{}, errors.New("can't get vaults"))

	// fail to get asgard vaults by status should result an error
	resp, err := querier.InboundAddresses(ctx, &types.QueryInboundAddressesRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	getVaultsCall.Unset()
	vaults := types.Vaults{
		GetRandomVault(),
		GetRandomVault(),
	}
	getVaultsCall = mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, nil).
		On("GetMostSecure", mock.Anything, mock.Anything, mock.Anything).Return(vaults[0])
	mimirCall := mockKeeper.On("GetMimir", mock.Anything, mock.Anything).Return(-1, nil).
		On("RagnarokInProgress", mock.Anything).Return(false)

	getNetworkFeeCall := mockKeeper.On("GetNetworkFee", mock.Anything, mock.Anything).Return(types.NetworkFee{
		Chain:              common.BNBChain,
		TransactionSize:    1,
		TransactionFeeRate: 37500,
	}, nil)

	resp, err = querier.InboundAddresses(ctx, &types.QueryInboundAddressesRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.True(t, len(resp.InboundAddresses) == 1)
	getNetworkFeeCall.Unset()
	mimirCall.Unset()
	getVaultsCall.Unset()
	// TODO add test cases to ensure that all `Halt` scenario works fine
}

func TestLiquidityProviders(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when fail to parse pool asset it should result in an error
	resp, err := querier.LiquidityProviders(ctx, &types.QueryLiquidityProvidersRequest{
		Asset: "whateverblabalba.b",
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when asset is synth asset , it should return an error, should tell user to user savers
	resp, err = querier.LiquidityProviders(ctx, &types.QueryLiquidityProvidersRequest{
		Asset: "BNB/BNB",
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	k := getInMemoryKeeper()
	k.SetLiquidityProvider(ctx, LiquidityProvider{
		Asset:              common.BNBAsset,
		RuneAddress:        GetRandomRUNEAddress(),
		AssetAddress:       GetRandomBNBAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(1000 * common.One),
		PendingRune:        cosmos.ZeroUint(),
		PendingAsset:       cosmos.ZeroUint(),
		PendingTxID:        "",
		RuneDepositValue:   cosmos.NewUint(1000 * common.One),
		AssetDepositValue:  cosmos.NewUint(1000 * common.One),
	})
	iterCall := mockKeeper.On("GetLiquidityProviderIterator", mock.Anything, common.BNBAsset).Return(k.GetLiquidityProviderIterator(ctx, common.BNBAsset)).
		On("Cdc").Return(k.Cdc())
	resp, err = querier.LiquidityProviders(ctx, &types.QueryLiquidityProvidersRequest{
		Asset: "BNB.BNB",
	})
	iterCall.Unset()
	require.NotNil(t, resp)
	require.Nil(t, err)
	require.Len(t, resp.LiquidityProviders, 1)
}

func TestLiquidityProvider(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	lpAddr := GetRandomTHORAddress()
	// when fail to parse pool asset it should result in an error
	resp, err := querier.LiquidityProvider(ctx, &types.QueryLiquidityProviderRequest{
		Asset:   "whateverblabalba.b",
		Address: lpAddr.String(),
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when asset is synth asset , it should return an error, should tell user to user savers
	resp, err = querier.LiquidityProvider(ctx, &types.QueryLiquidityProviderRequest{
		Asset:   "BNB/BNB",
		Address: lpAddr.String(),
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	lpCall := mockKeeper.On("GetLiquidityProvider", mock.Anything, common.BNBAsset, mock.Anything).Return(LiquidityProvider{}, errors.New("invalid LP"))

	resp, err = querier.LiquidityProvider(ctx, &types.QueryLiquidityProviderRequest{
		Asset:   "BNB.BNB",
		Address: lpAddr.String(),
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	lpCall.Unset()
	lp := LiquidityProvider{
		Asset:              common.BNBAsset,
		RuneAddress:        lpAddr,
		AssetAddress:       GetRandomBNBAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(1000 * common.One),
		PendingRune:        cosmos.ZeroUint(),
		PendingAsset:       cosmos.ZeroUint(),
		PendingTxID:        "",
		RuneDepositValue:   cosmos.NewUint(1000 * common.One),
		AssetDepositValue:  cosmos.NewUint(1000 * common.One),
	}
	lpCall = mockKeeper.On("GetLiquidityProvider", mock.Anything, common.BNBAsset, mock.Anything).Return(lp, nil)

	resp, err = querier.LiquidityProvider(ctx, &types.QueryLiquidityProviderRequest{
		Asset:   "BNB.BNB",
		Address: lpAddr.String(),
	})
	require.NotNil(t, resp)
	require.Nil(t, err)
	lpCall.Unset()
}

func TestSavers(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)

	// when fail to parse pool asset it should result in an error
	resp, err := querier.Savers(ctx, &types.QuerySaversRequest{
		Asset: "whateverblabalba.b",
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	k := getInMemoryKeeper()
	k.SetLiquidityProvider(ctx, LiquidityProvider{
		Asset:              common.BNBAsset,
		RuneAddress:        GetRandomRUNEAddress(),
		AssetAddress:       GetRandomBNBAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(1000 * common.One),
		PendingRune:        cosmos.ZeroUint(),
		PendingAsset:       cosmos.ZeroUint(),
		PendingTxID:        "",
		RuneDepositValue:   cosmos.NewUint(1000 * common.One),
		AssetDepositValue:  cosmos.NewUint(1000 * common.One),
	})
	iterCall := mockKeeper.On("GetLiquidityProviderIterator", mock.Anything, common.BNBAsset.GetSyntheticAsset()).Return(k.GetLiquidityProviderIterator(ctx, common.BNBAsset)).
		On("Cdc").Return(k.Cdc())
	resp, err = querier.Savers(ctx, &types.QuerySaversRequest{
		Asset: "BNB.BNB",
	})
	iterCall.Unset()
	require.NotNil(t, resp)
	require.Nil(t, err)
	require.Len(t, resp.Savers, 1)
}

func TestSaver(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	lpAddr := GetRandomTHORAddress()
	// when fail to parse pool asset it should result in an error
	resp, err := querier.Saver(ctx, &types.QuerySaverRequest{
		Asset:   "whateverblabalba.b",
		Address: lpAddr.String(),
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	lpCall := mockKeeper.On("GetLiquidityProvider", mock.Anything, common.BNBAsset.GetSyntheticAsset(), mock.Anything).Return(LiquidityProvider{}, errors.New("invalid LP"))

	resp, err = querier.Saver(ctx, &types.QuerySaverRequest{
		Asset:   "BNB.BNB",
		Address: lpAddr.String(),
	})
	require.Nil(t, resp)
	require.NotNil(t, err)

	lpCall.Unset()
	lp := LiquidityProvider{
		Asset:              common.BNBAsset,
		RuneAddress:        lpAddr,
		AssetAddress:       GetRandomBNBAddress(),
		LastAddHeight:      1024,
		LastWithdrawHeight: 0,
		Units:              cosmos.NewUint(1000 * common.One),
		PendingRune:        cosmos.ZeroUint(),
		PendingAsset:       cosmos.ZeroUint(),
		PendingTxID:        "",
		RuneDepositValue:   cosmos.NewUint(1000 * common.One),
		AssetDepositValue:  cosmos.NewUint(1000 * common.One),
	}
	lpCall = mockKeeper.On("GetLiquidityProvider", mock.Anything, common.BNBAsset.GetSyntheticAsset(), mock.Anything).Return(lp, nil)

	resp, err = querier.Saver(ctx, &types.QuerySaverRequest{
		Asset:   "BNB.BNB",
		Address: lpAddr.String(),
	})
	require.NotNil(t, resp)
	require.Nil(t, err)
	lpCall.Unset()
}

func TestTxVoters(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when tx id is empty , it should return an error
	resp, err := querier.TxVoters(ctx, &types.QueryTxVoterRequest{TxId: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when tx id is invalid , it should result in an error
	resp, err = querier.TxVoters(ctx, &types.QueryTxVoterRequest{TxId: "whatever"})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when fail to get txin voter , it should result an error
	inTxID := GetRandomTxHash()
	txInVoterCall := mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(types.ObservedTxVoter{}, errors.New("invalid tx voter"))
	resp, err = querier.TxVoters(ctx, &types.QueryTxVoterRequest{TxId: inTxID.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when tx in voter is present
	txInVoterCall.Unset()
	inTxVoter := GetRandomObservedTxVoter()
	inTxVoter.TxID = inTxID
	txInVoterCall = mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(inTxVoter, nil)
	resp, err = querier.TxVoters(ctx, &types.QueryTxVoterRequest{TxId: inTxID.String()})
	require.NotNil(t, resp)
	require.NotNil(t, resp.TxVoter)
	require.Nil(t, err)

	// when tx in voter is not present, it should try tx out
	txInVoterCall.Unset()
	txInVoterCall = mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(types.ObservedTxVoter{}, nil).
		On("GetObservedTxOutVoter", mock.Anything, inTxID).Return(inTxVoter, nil)
	resp, err = querier.TxVoters(ctx, &types.QueryTxVoterRequest{TxId: inTxID.String()})
	require.NotNil(t, resp)
	require.NotNil(t, resp.TxVoter)
	require.Nil(t, err)
	txInVoterCall.Unset()
}

func TestTx(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)

	// when tx id is empty , it should return an error
	resp, err := querier.Tx(ctx, &types.QueryTxRequest{TxId: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when tx id is invalid , it should return an error
	resp, err = querier.Tx(ctx, &types.QueryTxRequest{TxId: "invalid_tx_id"})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when fail to get txin voter , it should result an error
	inTxID := GetRandomTxHash()
	txInVoterCall := mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(types.ObservedTxVoter{}, errors.New("invalid tx voter"))
	resp, err = querier.Tx(ctx, &types.QueryTxRequest{TxId: inTxID.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when tx in voter is present
	txInVoterCall.Unset()
	inTxVoter := GetRandomObservedTxVoter()
	inTxVoter.TxID = inTxID
	txInVoterCall = mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(inTxVoter, nil)
	listActiveValidatorsCall := mockKeeper.On("ListActiveValidators", mock.Anything).Return(types.NodeAccounts{}, errors.New("fail to get active validators"))
	resp, err = querier.Tx(ctx, &types.QueryTxRequest{TxId: inTxID.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when tx in voter is present
	txInVoterCall.Unset()
	listActiveValidatorsCall.Unset()
	activeNodes := types.NodeAccounts{
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
	}
	inTxVoter.TxID = inTxID
	txInVoterCall = mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(inTxVoter, nil)
	mockKeeper.On("ListActiveValidators", mock.Anything).Return(activeNodes, nil)
	metricCall := mockKeeper.On("GetTssKeysignMetric", mock.Anything, mock.Anything).Return(&types.TssKeysignMetric{
		TxID: inTxID,
		NodeTssTimes: []types.NodeTssTime{
			{
				Address: activeNodes[0].NodeAddress,
				TssTime: 16,
			},
		},
	}, nil)
	resp, err = querier.Tx(ctx, &types.QueryTxRequest{TxId: inTxID.String()})
	require.NotNil(t, resp)
	require.NotNil(t, resp.ObservedTx)
	require.Nil(t, err)

	// when tx in voter is not present, it should try tx out
	txInVoterCall.Unset()
	txInVoterCall = mockKeeper.On("GetObservedTxInVoter", mock.Anything, inTxID).Return(types.ObservedTxVoter{}, nil).
		On("GetObservedTxOutVoter", mock.Anything, inTxID).Return(inTxVoter, nil)
	resp, err = querier.Tx(ctx, &types.QueryTxRequest{TxId: inTxID.String()})
	require.NotNil(t, resp)
	require.NotNil(t, resp.ObservedTx)
	require.Nil(t, err)
	metricCall.Unset()
	txInVoterCall.Unset()
}

func TestKeysigns(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockSigner, ok := querier.kbs.(*MockSigner)
	assert.True(t, ok)
	// when height is less than 0, it should result in an error
	resp, err := querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: -100})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when block height is larger than current block height , it should result in an error
	resp, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 102400})
	require.NotNil(t, err)
	require.Nil(t, resp)
	// when fail to get TxOut it should result in an error
	getTxOutCall := mockKeeper.On("GetTxOut", mock.Anything, int64(5)).Return(NewTxOut(5), errors.New("fail to get tx out"))
	resp, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	require.NotNil(t, err)
	require.Nil(t, resp)

	pk := GetRandomPubKey()
	toAddr := GetRandomBNBAddress()
	txOut := NewTxOut(5)
	txOutItem := types.TxOutItem{
		Chain:       common.BNBChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(100*common.One)),
	}
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	getTxOutCall.Unset()
	mockKeeper.On("GetTxOut", mock.Anything, int64(5)).Return(txOut, nil)

	// when fail to sign , it should result in an error
	signCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, errors.New("fail to sign"))
	resp, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	require.NotNil(t, err)
	require.Nil(t, resp)
	signCall.Unset()

	signCall = mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, nil)
	resp, err = querier.Keysigns(ctx, &types.QueryKeysignsRequest{Height: 5})
	require.Nil(t, err)
	require.NotNil(t, resp)
	signCall.Unset()
}

func TestKeysignsByPubKey(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockSigner, ok := querier.kbs.(*MockSigner)
	assert.True(t, ok)
	// when height is less than 0, it should result in an error
	resp, err := querier.KeysignsByPubKey(ctx, &types.QueryKeysignsByPubKeyRequest{
		Height: -100,
		PubKey: "",
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when block height is larger than current block height , it should result in an error
	resp, err = querier.KeysignsByPubKey(ctx, &types.QueryKeysignsByPubKeyRequest{
		Height: 102400,
		PubKey: "",
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to get TxOut it should result in an error
	getTxOutCall := mockKeeper.On("GetTxOut", mock.Anything, int64(5)).Return(NewTxOut(5), errors.New("fail to get tx out"))
	resp, err = querier.KeysignsByPubKey(ctx, &types.QueryKeysignsByPubKeyRequest{
		Height: 5,
		PubKey: "",
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	pk := GetRandomPubKey()
	toAddr := GetRandomBNBAddress()
	txOut := NewTxOut(5)
	txOutItem := types.TxOutItem{
		Chain:       common.BNBChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(100*common.One)),
	}
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	getTxOutCall.Unset()
	mockKeeper.On("GetTxOut", mock.Anything, int64(5)).Return(txOut, nil)

	// when fail to sign , it should result in an error
	signCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, errors.New("fail to sign"))
	resp, err = querier.KeysignsByPubKey(ctx, &types.QueryKeysignsByPubKeyRequest{
		Height: 5,
		PubKey: pk.String(),
	})
	require.NotNil(t, err)
	require.Nil(t, resp)
	signCall.Unset()

	signCall = mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, nil)
	resp, err = querier.KeysignsByPubKey(ctx, &types.QueryKeysignsByPubKeyRequest{
		Height: 5,
		PubKey: pk.String(),
	})
	require.Nil(t, err)
	require.NotNil(t, resp)
	signCall.Unset()
}

func TestKeygens(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockSigner, ok := querier.kbs.(*MockSigner)
	assert.True(t, ok)
	// when height is less than 0, it should result in an error
	resp, err := querier.Keygens(ctx, &types.QueryKeygensRequest{
		Height: -100,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when block height is larger than current block height , it should result in an error
	resp, err = querier.Keygens(ctx, &types.QueryKeygensRequest{
		Height: 102400,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to get keygen block , it should result in an error
	getKeygenBlockCall := mockKeeper.On("GetKeygenBlock", mock.Anything, int64(5)).Return(types.KeygenBlock{}, errors.New("fail to create keygen block"))
	resp, err = querier.Keygens(ctx, &types.QueryKeygensRequest{
		Height: 5,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to sign should result in an error
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	getKeygenBlockCall.Unset()
	keygenBlock := NewKeygenBlock(5)
	keygen, err := NewKeygen(5, []string{
		GetRandomRUNEAddress().String(),
		GetRandomRUNEAddress().String(),
	}, AsgardKeygen)
	require.Nil(t, err)
	keygenBlock.Keygens = append(keygenBlock.Keygens, keygen)
	mockKeeper.On("GetKeygenBlock", mock.Anything, int64(5)).Return(keygenBlock, nil)
	signCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, errors.New("fail to sign"))
	resp, err = querier.Keygens(ctx, &types.QueryKeygensRequest{
		Height: 5,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// normal happy path
	signCall.Unset()
	signCall = mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, nil)
	resp, err = querier.Keygens(ctx, &types.QueryKeygensRequest{
		Height: 5,
	})
	require.Nil(t, err)
	require.NotNil(t, resp)
	signCall.Unset()
}

func TestKeygensByPubKey(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockSigner, ok := querier.kbs.(*MockSigner)
	assert.True(t, ok)
	// when height is less than 0, it should result in an error
	resp, err := querier.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: -100,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when block height is larger than current block height , it should result in an error
	resp, err = querier.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: 102400,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to get keygen block , it should result in an error
	getKeygenBlockCall := mockKeeper.On("GetKeygenBlock", mock.Anything, int64(5)).Return(types.KeygenBlock{}, errors.New("fail to create keygen block"))
	resp, err = querier.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: 5,
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to sign should result in an error
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	getKeygenBlockCall.Unset()
	keygenBlock := NewKeygenBlock(5)
	keygen, err := NewKeygen(5, []string{
		GetRandomPubKey().String(),
		GetRandomPubKey().String(),
	}, AsgardKeygen)
	require.Nil(t, err)
	keygenBlock.Keygens = append(keygenBlock.Keygens, keygen)
	mockKeeper.On("GetKeygenBlock", mock.Anything, int64(5)).Return(keygenBlock, nil)
	signCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, errors.New("fail to sign"))
	resp, err = querier.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: 5,
		PubKey: keygen.Members[0],
	})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// normal happy path
	signCall.Unset()
	signCall = mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, nil)
	resp, err = querier.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: 5,
		PubKey: keygen.Members[0],
	})
	require.Nil(t, err)
	require.NotNil(t, resp)
	signCall.Unset()
}

func TestQueue(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	// add a swap
	assert.Nil(t, k.SetSwapQueueItem(ctx, *types.NewMsgSwap(
		GetRandomTx(),
		common.BNBAsset,
		GetRandomBNBAddress(),
		cosmos.ZeroUint(),
		common.NoAddress,
		cosmos.ZeroUint(),
		"", "", nil, LimitOrder, GetRandomBech32Addr(),
	), 1))
	mockSwapIter := mockKeeper.On("GetSwapQueueIterator", mock.Anything).Return(k.GetSwapQueueIterator(ctx))
	mockKeeper.On("Cdc").Return(k.Cdc()).
		On("GetOrderBookItemIterator", mock.Anything).Return(k.GetOrderBookItemIterator(ctx)).
		On("GetMimir", mock.Anything, mock.Anything).Return(-1, nil).
		On("GetTxOutValue", mock.Anything, mock.Anything).Return(cosmos.ZeroUint(), nil)
	mockGetTxOut := mockKeeper.On("GetTxOut", mock.Anything, mock.Anything).Return(NewTxOut(1), errors.New("fail to get tx out"))
	resp, err := querier.Queue(ctx, &types.QueryQueueRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	mockGetTxOut.Unset()
	mockSwapIter.Unset()

	mockSwapIter = mockKeeper.On("GetSwapQueueIterator", mock.Anything).Return(k.GetSwapQueueIterator(ctx))
	mockGetTxOut = mockKeeper.On("GetTxOut", mock.Anything, mock.Anything).Return(NewTxOut(1), nil)

	resp, err = querier.Queue(ctx, &types.QueryQueueRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.True(t, resp.Swap == 1)
	require.True(t, resp.Outbound == 0)
	require.True(t, resp.Internal == 0)
	mockGetTxOut.Unset()
	mockSwapIter.Unset()
}

func TestOutbound(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when fail to get txout , it should result in an error
	mockGetTxOut := mockKeeper.On("GetTxOut", mock.Anything, mock.Anything).Return(NewTxOut(1), errors.New("fail to get tx out"))
	resp, err := querier.Outbound(ctx, &types.QueryOutboundRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)
	mockGetTxOut.Unset()

	pk := GetRandomPubKey()
	toAddr := GetRandomBNBAddress()
	txOut := NewTxOut(10)
	txOutItem := types.TxOutItem{
		Chain:       common.BNBChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(100*common.One)),
	}
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	mockGetTxOut = mockKeeper.On("GetTxOut", mock.Anything, mock.Anything).Return(txOut, nil)
	resp, err = querier.Outbound(ctx, &types.QueryOutboundRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.Len(t, resp.TxOutItems, 19)
	mockGetTxOut.Unset()
}

func TestScheduledOutbound(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockMimir := mockKeeper.On("GetMimir", mock.Anything, mock.Anything).Return(-1, nil)

	pk := GetRandomPubKey()
	toAddr := GetRandomBNBAddress()
	txOut := NewTxOut(10)
	txOutItem := types.TxOutItem{
		Chain:       common.BNBChain,
		VaultPubKey: pk,
		ToAddress:   toAddr,
		InHash:      GetRandomTxHash(),
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(100*common.One)),
	}
	txOut.TxArray = append(txOut.TxArray, txOutItem)
	mockGetTxOut := mockKeeper.On("GetTxOut", mock.Anything, mock.Anything).Return(txOut, nil)
	resp, err := querier.ScheduledOutbound(ctx, &types.QueryScheduledOutboundRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.Len(t, resp.TxOutItems, 17280)
	mockMimir.Unset()
	mockGetTxOut.Unset()
}

func TestHeights(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)

	// fail to get asgards should result in an error
	mockAsgard := mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(types.Vaults{}, errors.New("fail to get asgard"))
	resp, err := querier.Heights(ctx, &types.QueryHeightsRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// fail to get last chain height should result in an error
	mockAsgard.Unset()

	vaults := types.Vaults{
		GetRandomVault(),
		GetRandomVault(),
	}
	mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, nil)
	mockGetLastChainHeight := mockKeeper.On("GetLastChainHeight", mock.Anything, mock.Anything).Return(1024, errors.New("invalid chain height"))
	resp, err = querier.Heights(ctx, &types.QueryHeightsRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// fail to get last signed height should result in an error
	mockGetLastChainHeight.Unset()
	mockKeeper.On("GetLastChainHeight", mock.Anything, mock.Anything).Return(1024, nil)
	mockLastSignedHeight := mockKeeper.On("GetLastSignedHeight", mock.Anything).Return(2048, errors.New("fail to get last signed height"))
	resp, err = querier.Heights(ctx, &types.QueryHeightsRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// happy path
	mockLastSignedHeight.Unset()
	mockLastSignedHeight = mockKeeper.On("GetLastSignedHeight", mock.Anything).Return(2048, nil)
	resp, err = querier.Heights(ctx, &types.QueryHeightsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	mockLastSignedHeight.Unset()
}

func TestChainHeights(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)

	// when chain is empty , it should result in an error
	resp, err := querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when chain is invalid , it should result in an error
	resp, err = querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: "asfasdfasdfasdfasdfasdfasd"})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when chain is thorchain , it should return empty result
	resp, err = querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: "THOR"})
	require.Nil(t, resp)
	require.Nil(t, err)

	mockGetLastChainHeight := mockKeeper.On("GetLastChainHeight", mock.Anything, mock.Anything).Return(1024, errors.New("invalid chain height"))
	resp, err = querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: "BNB"})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// fail to get last signed height should result in an error
	mockGetLastChainHeight.Unset()
	mockKeeper.On("GetLastChainHeight", mock.Anything, mock.Anything).Return(1024, nil)
	mockLastSignedHeight := mockKeeper.On("GetLastSignedHeight", mock.Anything).Return(2048, errors.New("fail to get last signed height"))
	resp, err = querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: "BNB"})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// happy path
	mockLastSignedHeight.Unset()
	mockKeeper.On("GetLastSignedHeight", mock.Anything).Return(2048, nil)
	resp, err = querier.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: "BNB"})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestNodes(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// fail to get nodes should result in an error
	mockListValWithBond := mockKeeper.On("ListValidatorsWithBond", mock.Anything).Return(types.NodeAccounts{}, errors.New("fail to get validators with bond"))
	resp, err := querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	mockListValWithBond.Unset()

	nodes := types.NodeAccounts{
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
	}
	// fail to get all active nodes account should result in an error
	mockKeeper.On("ListValidatorsWithBond", mock.Anything).Return(nodes, nil)
	mockListActiveValidators := mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, errors.New("fail to list active validators"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get network should result in an error
	mockListActiveValidators.Unset()
	mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, nil)
	mockNetwork := mockKeeper.On("GetNetwork", mock.Anything).Return(types.NewNetwork(), errors.New("fail to get network"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get active vaults should result in an error
	mockNetwork.Unset()
	mockKeeper.On("GetNetwork", mock.Anything).Return(types.NewNetwork(), nil)
	vault1 := GetRandomVault()
	vault2 := GetRandomVault()
	vaults := types.Vaults{
		vault1, vault2,
	}
	mockGetAsgard := mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, errors.New("fail to get asgard vaults"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get totalEffectiveBond should result an error
	mockGetAsgard.Unset()
	mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, nil)
	mockGetValidatorsByStatus := mockKeeper.On("ListValidatorsByStatus", mock.Anything, NodeActive).Return(nodes, errors.New("fail to list validators by status"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get slash points should result in an error
	mockGetValidatorsByStatus.Unset()
	mockKeeper.On("ListValidatorsByStatus", mock.Anything, NodeActive).Return(nodes, nil)
	mcokNodeAccountSlashPoints := mockKeeper.On("GetNodeAccountSlashPoints", mock.Anything, mock.Anything).Return(1024, errors.New("fail to get slash points"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get node account jail status should result in an error
	mcokNodeAccountSlashPoints.Unset()
	mockKeeper.On("GetNodeAccountSlashPoints", mock.Anything, mock.Anything).Return(1024, nil)
	mockJail := mockKeeper.On("GetNodeAccountJail", mock.Anything, mock.Anything).Return(types.NewJail(nodes[0].NodeAddress), errors.New("fail to get node jail"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get last observe height should result in an error
	mockJail.Unset()
	mockKeeper.On("GetNodeAccountJail", mock.Anything, mock.Anything).Return(types.NewJail(nodes[0].NodeAddress), nil)
	mockGetLastObserveHeight := mockKeeper.On("GetLastObserveHeight", mock.Anything, mock.Anything).Return(map[common.Chain]int64{}, errors.New("fail to get last observe height"))
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockGetLastObserveHeight.Unset()
	mockKeeper.On("GetLastObserveHeight", mock.Anything, mock.Anything).Return(map[common.Chain]int64{
		common.BNBChain: 1024,
		common.BTCChain: 100,
	}, nil)
	mockKeeper.On("GetBondProviders", mock.Anything, mock.Anything).Return(types.BondProviders{}, nil)
	resp, err = querier.Nodes(ctx, &types.QueryNodesRequest{})
	require.NotNil(t, resp)
	require.Nil(t, err)
}

func TestNode(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when node address is empty, it should result in an error
	resp, err := querier.Node(ctx, &types.QueryNodeRequest{Address: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when node address is invalid , it should result in an error
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: "whatever"})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get node account , it should result in an error
	node := GetRandomValidatorNode(NodeActive)
	mockNodeAccount := mockKeeper.On("GetNodeAccount", mock.Anything, mock.Anything).Return(node, errors.New("fail to get node account"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: "whatever"})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get slash points , it should result in an error
	mockNodeAccount.Unset()
	mockKeeper.On("GetNodeAccount", mock.Anything, mock.Anything).Return(node, nil)
	mcokNodeAccountSlashPoints := mockKeeper.On("GetNodeAccountSlashPoints", mock.Anything, mock.Anything).Return(1024, errors.New("fail to get slash points"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get jail status, it should result in an error
	mcokNodeAccountSlashPoints.Unset()
	mockKeeper.On("GetNodeAccountSlashPoints", mock.Anything, mock.Anything).Return(1024, nil)
	mockJail := mockKeeper.On("GetNodeAccountJail", mock.Anything, mock.Anything).Return(types.NewJail(node.NodeAddress), errors.New("fail to get node jail"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get bond provider ,it should result in an error
	mockJail.Unset()
	mockKeeper.On("GetNodeAccountJail", mock.Anything, mock.Anything).Return(types.NewJail(node.NodeAddress), nil)
	mockBondProviders := mockKeeper.On("GetBondProviders", mock.Anything, mock.Anything).Return(types.BondProviders{}, errors.New("fail to get bond providers"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to list active validators , it should result in an error
	mockBondProviders.Unset()
	nodes := types.NodeAccounts{
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
	}
	mockKeeper.On("GetBondProviders", mock.Anything, mock.Anything).Return(types.BondProviders{}, nil)
	mockListActiveValidators := mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, errors.New("fail to list active validators"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get network, it should result in an error
	mockListActiveValidators.Unset()
	mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, nil)
	mockNetwork := mockKeeper.On("GetNetwork", mock.Anything).Return(types.NewNetwork(), errors.New("fail to get network"))
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// fail to get asgard , it should result in an error
	mockNetwork.Unset()
	mockKeeper.On("GetNetwork", mock.Anything).Return(types.NewNetwork(), nil)
	vault1 := GetRandomVault()
	vault2 := GetRandomVault()
	vaults := types.Vaults{
		vault1, vault2,
	}
	mockGetAsgard := mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, errors.New("fail to get asgard vaults"))

	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// when fail to get last observe height , it should result in an error
	mockGetAsgard.Unset()
	mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, nil)
	mockGetLastObserveHeight := mockKeeper.On("GetLastObserveHeight", mock.Anything, mock.Anything).Return(map[common.Chain]int64{}, errors.New("fail to get last observe height"))
	mockGetValidatorsByStatus := mockKeeper.On("ListValidatorsByStatus", mock.Anything, NodeActive).Return(nodes, nil)
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// happy path
	mockGetLastObserveHeight.Unset()
	mockGetLastObserveHeight = mockKeeper.On("GetLastObserveHeight", mock.Anything, mock.Anything).Return(map[common.Chain]int64{
		common.BNBChain: 1024,
		common.BTCChain: 100,
	}, nil)
	resp, err = querier.Node(ctx, &types.QueryNodeRequest{Address: node.NodeAddress.String()})
	require.NotNil(t, resp)
	require.Nil(t, err)
	mockGetValidatorsByStatus.Unset()
	mockGetLastObserveHeight.Unset()
}

func TestNetwork(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// fail to get network should result in an error
	mockNetwork := mockKeeper.On("GetNetwork", mock.Anything).Return(types.NewNetwork(), errors.New("fail to get network"))
	resp, err := querier.Network(ctx, &types.QueryNetworkRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockNetwork.Unset()
	network := types.NewNetwork()
	mockKeeper.On("GetRuneBalanceOfModule", mock.Anything, ReserveName).Return(cosmos.NewUint(1024))
	mockKeeper.On("GetNetwork", mock.Anything).Return(network, nil)
	resp, err = querier.Network(ctx, &types.QueryNetworkRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestPOL(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// fail to get POL should result in an error
	mockPol := mockKeeper.On("GetPOL", mock.Anything).Return(types.NewProtocolOwnedLiquidity(), errors.New("fail to get POL"))
	resp, err := querier.POL(ctx, &types.QueryPOLRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// fail to get pool value should result in an error
	mockPol.Unset()
	pol := types.NewProtocolOwnedLiquidity()
	mockModuleAddr := mockKeeper.On("GetModuleAddress", ReserveName).Return(GetRandomTHORAddress(), errors.New("fail to get address"))
	mockKeeper.On("GetPOL", mock.Anything).Return(pol, nil)
	resp, err = querier.POL(ctx, &types.QueryPOLRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get pools should result in an error
	mockModuleAddr.Unset()
	mockKeeper.On("GetModuleAddress", ReserveName).Return(GetRandomTHORAddress(), nil)
	mockPools := mockKeeper.On("GetPools", mock.Anything).Return(types.Pools{}, errors.New("fail to get pools"))
	resp, err = querier.POL(ctx, &types.QueryPOLRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// fail to get LP should result in an error
	mockPools.Unset()
	poolBnb := types.NewPool()
	poolBnb.Asset = common.BNBAsset
	poolBnb.BalanceAsset = cosmos.NewUint(1000 * common.One)
	poolBnb.BalanceRune = cosmos.NewUint(1000 * common.One)
	mockKeeper.On("GetPools", mock.Anything).Return(types.Pools{
		poolBnb,
	}, nil)
	mockKeeper.On("GetTotalSupply", mock.Anything, mock.Anything).Return(cosmos.NewUint(100 * common.One))
	mockLP := mockKeeper.On("GetLiquidityProvider", mock.Anything, mock.Anything, mock.Anything).Return(types.LiquidityProvider{}, errors.New("fail to get LP"))
	resp, err = querier.POL(ctx, &types.QueryPOLRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockLP.Unset()
	lp := types.LiquidityProvider{
		Asset:             common.BNBAsset,
		RuneAddress:       GetRandomRUNEAddress(),
		AssetAddress:      GetRandomBNBAddress(),
		LastAddHeight:     1024,
		Units:             cosmos.NewUint(100 * common.One),
		PendingRune:       cosmos.ZeroUint(),
		PendingAsset:      cosmos.ZeroUint(),
		PendingTxID:       "",
		RuneDepositValue:  cosmos.NewUint(100 * common.One),
		AssetDepositValue: cosmos.NewUint(100 * common.One),
	}
	mockKeeper.On("GetLiquidityProvider", mock.Anything, mock.Anything, mock.Anything).Return(lp, nil)
	resp, err = querier.POL(ctx, &types.QueryPOLRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestModuleBalance(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockKeeper.On("GetModuleAccAddress", mock.Anything).Return(GetRandomBech32Addr())
	mockKeeper.On("GetBalance", mock.Anything, mock.Anything).Return(cosmos.NewCoins(
		cosmos.NewCoin("BNB.BNB", cosmos.NewInt(1000*common.One))))
	resp, err := querier.ModuleBalance(ctx, &types.QueryModuleBalanceRequest{Name: ReserveName})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestAsgards(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// fail to get asgards, it should result in an error
	mockAsgards := mockKeeper.On("GetAsgardVaults", mock.Anything).Return(types.Vaults{}, errors.New("fail to get asgards"))
	resp, err := querier.Asgards(ctx, &types.QueryAsgardsRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockAsgards.Unset()
	vault1 := GetRandomVault()
	vault1.Coins = common.Coins{
		common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		common.NewCoin(common.BTCAsset, cosmos.NewUint(102400)),
	}
	vault2 := GetRandomVault()
	vault2.Coins = common.Coins{
		common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		common.NewCoin(common.BTCAsset, cosmos.NewUint(102400)),
	}
	vault3 := GetRandomVault()
	vault3.Status = InactiveVault
	vault4 := GetRandomVault()
	vault4.Type = types.VaultType_UnknownVault
	vaults := types.Vaults{
		vault1, vault2, vault3, vault4,
	}
	mockKeeper.On("GetAsgardVaults", mock.Anything).Return(vaults, nil)
	resp, err = querier.Asgards(ctx, &types.QueryAsgardsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestVault(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when pubkey is empty, it should result in an error
	resp, err := querier.Vault(ctx, &types.QueryVaultRequest{PubKey: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// invalid pubkey should result in an error
	resp, err = querier.Vault(ctx, &types.QueryVaultRequest{PubKey: "whatever"})
	require.Nil(t, resp)
	require.NotNil(t, err)
	vault1 := GetRandomVault()
	vault1.Coins = common.Coins{
		common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		common.NewCoin(common.BTCAsset, cosmos.NewUint(102400)),
	}
	// fail to get vault should result in an error
	mockVault := mockKeeper.On("GetVault", mock.Anything, mock.Anything).Return(vault1, errors.New("fail to get vault"))
	resp, err = querier.Vault(ctx, &types.QueryVaultRequest{PubKey: vault1.PubKey.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// vault can't find should result in an error
	mockVault.Unset()
	mockVault = mockKeeper.On("GetVault", mock.Anything, mock.Anything).Return(types.Vault{}, nil)
	resp, err = querier.Vault(ctx, &types.QueryVaultRequest{PubKey: vault1.PubKey.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockVault.Unset()
	mockKeeper.On("GetVault", mock.Anything, mock.Anything).Return(vault1, nil)
	resp, err = querier.Vault(ctx, &types.QueryVaultRequest{PubKey: vault1.PubKey.String()})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestVaultPubKeys(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	vault1 := GetRandomVault()
	vault1.Coins = common.Coins{
		common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		common.NewCoin(common.BTCAsset, cosmos.NewUint(102400)),
	}
	assert.Nil(t, k.SetVault(ctx, vault1))
	mockKeeper.On("GetVaultIterator", mock.Anything).Return(k.GetVaultIterator(ctx)).
		On("Cdc").Return(k.Cdc())
	resp, err := querier.VaultPubkeys(ctx, &types.QueryVaultPubKeysRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestConstants(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	resp, err := querier.Constants(ctx, &types.QueryConstantsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestMimirs(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	k.SetMimir(ctx, constants.MaxSynthPerPoolDepth.String(), 100)
	mockKeeper.On("GetMimirIterator", mock.Anything).Return(k.GetMimirIterator(ctx))
	// when get mimir fail, it should result in an error
	mockMimir := mockKeeper.On("GetMimir", mock.Anything, mock.Anything).Return(-1, errors.New("fail to get mimir"))
	resp, err := querier.Mimirs(ctx, &types.QueryMimirsRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// happy path
	mockMimir.Unset()
	mockKeeper.On("GetMimir", mock.Anything, mock.Anything).Return(1024, nil)
	resp, err = querier.Mimirs(ctx, &types.QueryMimirsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_MimirWithKey(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when name is empty , it should result in an error
	resp, err := querier.MimirWithKey(ctx, &types.QueryMimirWithKeyRequest{Name: ""})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when fail to get mimir , it should result in an error
	mockMimir := mockKeeper.On("GetMimir", mock.Anything, "test-name").Return(100, errors.New("fail to get mimir"))
	resp, err = querier.MimirWithKey(ctx, &types.QueryMimirWithKeyRequest{Name: "test-name"})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// happy path
	mockMimir.Unset()
	mockKeeper.On("GetMimir", mock.Anything, "test-name").Return(100, nil)
	resp, err = querier.MimirWithKey(ctx, &types.QueryMimirWithKeyRequest{Name: "test-name"})
	require.NotNil(t, resp)
	require.Nil(t, err)
}

func TestGRPCQuerier_AdminMimirs(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	k.SetMimir(ctx, "whatever", 1234)
	mockKeeper.On("GetMimirIterator", mock.Anything).Return(k.GetMimirIterator(ctx)).
		On("Cdc").Return(k.Cdc())
	resp, err := querier.AdminMimirs(ctx, &types.QueryAdminMimirRequest{})
	require.NotNil(t, resp)
	require.Nil(t, err)
}

func TestGRPCQuerier_NodesMimir(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	// when fail to list active validators should result in an error
	nodes := types.NodeAccounts{
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
		GetRandomValidatorNode(NodeActive),
	}
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1, nodes[0].NodeAddress))
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1, nodes[1].NodeAddress))
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1, nodes[2].NodeAddress))
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1, nodes[3].NodeAddress))
	mockListActiveValidator := mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, errors.New("fail to list active validators"))
	resp, err := querier.NodesMimir(ctx, &types.QueryNodeMimirRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// happy path
	mockListActiveValidator.Unset()
	mockKeeper.On("ListActiveValidators", mock.Anything).Return(nodes, nil)
	mockKeeper.On("GetNodeMimirIterator", mock.Anything).Return(k.GetNodeMimirIterator(ctx)).
		On("Cdc").Return(k.Cdc())
	resp, err = querier.NodesMimir(ctx, &types.QueryNodeMimirRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_NodeMimirWithAddress(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	// when address is invalid , it should result in an error
	resp, err := querier.NodeMimirWithAddress(ctx, &types.QueryNodeMimirWithAddressRequest{Address: ""})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// happy path
	nodeAddr := GetRandomBech32Addr()
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1024, nodeAddr))
	mockIter := mockKeeper.On("GetNodeMimirIterator", mock.Anything).Return(k.GetNodeMimirIterator(ctx)).
		On("Cdc").Return(k.Cdc())
	resp, err = querier.NodeMimirWithAddress(ctx, &types.QueryNodeMimirWithAddressRequest{Address: nodeAddr.String()})
	require.Nil(t, err)
	require.NotNil(t, resp)
	mockIter.Unset()
}

func TestGRPCQuerier_AllNodeMimirs(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	k := getInMemoryKeeper()
	nodeAddr := GetRandomBech32Addr()
	assert.Nil(t, k.SetNodeMimir(ctx, "whatever", 1024, nodeAddr))
	mockKeeper.On("GetNodeMimirIterator", mock.Anything).Return(k.GetNodeMimirIterator(ctx)).
		On("Cdc").Return(k.Cdc())
	resp, err := querier.AllNodeMimirs(ctx, &types.QueryAllNodesMimirsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_Ban(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when address is empty it should result in an error
	resp, err := querier.Ban(ctx, &types.QueryBanRequest{Address: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)

	nodeAddr := GetRandomBech32Addr()
	// fail to get ban voter should result in an error
	mockGetBanVoter := mockKeeper.On("GetBanVoter", mock.Anything, mock.Anything).Return(types.BanVoter{}, errors.New("fail to get ban voter"))
	resp, err = querier.Ban(ctx, &types.QueryBanRequest{Address: nodeAddr.String()})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// happy path
	mockGetBanVoter.Unset()
	bv := types.NewBanVoter(nodeAddr)
	mockKeeper.On("GetBanVoter", mock.Anything, mock.Anything).Return(bv, nil)
	resp, err = querier.Ban(ctx, &types.QueryBanRequest{Address: nodeAddr.String()})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_Ragnarok(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockKeeper.On("RagnarokInProgress", mock.Anything).Return(true)
	resp, err := querier.Ragnarok(ctx, &types.QueryRagnarokRequest{})
	require.NotNil(t, resp)
	require.Nil(t, err)
}

func TestGRPCQuerier_KeygenMetric(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when pubkey is invalid , it should result in an error
	resp, err := querier.KeygenMetric(ctx, &types.QueryKeygenMetricRequest{PubKey: "whatever"})
	require.NotNil(t, err)
	require.Nil(t, resp)
	// fail to get keygen metric should result in an error
	mockKeygenMetric := mockKeeper.On("GetTssKeygenMetric", mock.Anything, mock.Anything).Return(&types.TssKeygenMetric{}, errors.New("fail to get keygen metric"))
	resp, err = querier.KeygenMetric(ctx, &types.QueryKeygenMetricRequest{PubKey: GetRandomPubKey().String()})
	require.NotNil(t, err)
	require.Nil(t, resp)
	// happy path
	mockKeygenMetric.Unset()
	mockKeeper.On("GetTssKeygenMetric", mock.Anything, mock.Anything).Return(&types.TssKeygenMetric{
		PubKey: GetRandomPubKey(),
		NodeTssTimes: []types.NodeTssTime{
			{
				Address: GetRandomBech32Addr(),
				TssTime: 10,
			},
		},
	}, nil)
	resp, err = querier.KeygenMetric(ctx, &types.QueryKeygenMetricRequest{PubKey: GetRandomPubKey().String()})
	require.NotNil(t, resp)
	require.Nil(t, err)
}

func TestGRPCQuerier_TssMetric(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// fail to get asgard vault should result in an error
	vaults := types.Vaults{
		GetRandomVault(),
		GetRandomVault(),
	}
	mockVault := mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, errors.New("fail to get asgard vault"))
	resp, err := querier.TssMetric(ctx, &types.QueryTssMetricRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)
	//  fail to get keygen metric should result in an error
	mockVault.Unset()
	mockKeeper.On("GetAsgardVaultsByStatus", mock.Anything, ActiveVault).Return(vaults, nil)
	mockKeygenMetric := mockKeeper.On("GetTssKeygenMetric", mock.Anything, mock.Anything).Return(&types.TssKeygenMetric{}, errors.New("fail to get keygen metric"))
	resp, err = querier.TssMetric(ctx, &types.QueryTssMetricRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)
	// fail to get latest keysign metric should result in an error
	mockKeygenMetric.Unset()
	mockKeeper.On("GetTssKeygenMetric", mock.Anything, mock.Anything).Return(&types.TssKeygenMetric{
		PubKey: GetRandomPubKey(),
		NodeTssTimes: []types.NodeTssTime{
			{
				Address: GetRandomBech32Addr(),
				TssTime: 10,
			},
		},
	}, nil)
	mockLastKeysign := mockKeeper.On("GetLatestTssKeysignMetric", mock.Anything).Return(&keeper.TssKeysignMetric{}, errors.New("fail to get tss key sign"))
	resp, err = querier.TssMetric(ctx, &types.QueryTssMetricRequest{})
	require.Nil(t, resp)
	require.NotNil(t, err)
	// happy path
	mockLastKeysign.Unset()
	mockKeeper.On("GetLatestTssKeysignMetric", mock.Anything).Return(&keeper.TssKeysignMetric{
		TxID: GetRandomTxHash(),
		NodeTssTimes: []types.NodeTssTime{
			{
				Address: GetRandomBech32Addr(),
				TssTime: 10,
			},
		},
	}, nil)
	resp, err = querier.TssMetric(ctx, &types.QueryTssMetricRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_Thorname(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when name is empty it should result in an error
	resp, err := querier.Thorname(ctx, &types.QueryThornameRequest{Name: ""})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// when fail to get thorname , it should result in an error
	mockThorName := mockKeeper.On("GetTHORName", mock.Anything, mock.Anything).Return(types.THORName{}, errors.New("fail to get thorname"))
	resp, err = querier.Thorname(ctx, &types.QueryThornameRequest{Name: "whatever"})
	require.Nil(t, resp)
	require.NotNil(t, err)

	// happy path
	mockThorName.Unset()
	mockKeeper.On("GetTHORName", mock.Anything, mock.Anything).Return(types.THORName{
		Name:              "whatever",
		ExpireBlockHeight: 1024,
		Owner:             GetRandomBech32Addr(),
		PreferredAsset:    common.BNBAsset,
		Aliases: []types.THORNameAlias{
			{
				Chain:   common.BNBChain,
				Address: GetRandomBNBAddress(),
			},
		},
	}, nil)
	resp, err = querier.Thorname(ctx, &types.QueryThornameRequest{Name: "whatever"})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_Version(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockKeeper.On("GetVersionWithCtx", mock.Anything).Return(semver.MustParse("1.100.0"), true).
		On("GetMinJoinVersion", mock.Anything).Return(semver.MustParse("1.100.0"))

	resp, err := querier.Version(ctx, &types.QueryVersionRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
}

func TestGRPCQuerier_Claims(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when fail to get BlockClaims , it should result in an error
	mockGetBlockClaims := mockKeeper.On("GetBlockClaims", mock.Anything, mock.Anything).Return(types.NewBlockClaims(1), errors.New("fail to get tx out"))
	resp, err := querier.Claims(ctx, &types.QueryClaimsRequest{})
	require.NotNil(t, err)
	require.Nil(t, resp)
	mockGetBlockClaims.Unset()

	blockClaims := types.NewBlockClaims(10)
	claimItem := types.ClaimItem{
		From:      GetRandomBech32Addr().String(),
		To:        GetRandomBech32Addr().String(),
		Signature: "Signature",
		TxID:      GetRandomTxHash(),
		Message:   "HelloWorld",
		Signer:    GetRandomBech32Addr().String(),
	}
	blockClaims.Claims = append(blockClaims.Claims, claimItem)
	mockGetBlockClaims = mockKeeper.On("GetBlockClaims", mock.Anything, mock.Anything).Return(blockClaims, nil)
	resp, err = querier.Claims(ctx, &types.QueryClaimsRequest{})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.Len(t, resp.Claims, 19)
	mockGetBlockClaims.Unset()
}

func TestGRPCQuerier_ClaimsByHeight(t *testing.T) {
	ctx, querier := GetQuerierForTest()
	mockKeeper, ok := querier.mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	mockSigner, ok := querier.kbs.(*MockSigner)
	assert.True(t, ok)
	// when height is less than 0, it should result in an error
	resp, err := querier.ClaimsByHeight(ctx, &types.QueryClaimsByHeightRequest{Height: -100})
	require.NotNil(t, err)
	require.Nil(t, resp)

	// when block height is larger than current block height , it should result in an error
	resp, err = querier.ClaimsByHeight(ctx, &types.QueryClaimsByHeightRequest{Height: 102400})
	require.NotNil(t, err)
	require.Nil(t, resp)
	// when fail to get TxOut it should result in an error
	getBlockClaimsCall := mockKeeper.On("GetBlockClaims", mock.Anything, int64(5)).Return(types.NewBlockClaims(5), errors.New("fail to get BlockClaims"))
	resp, err = querier.ClaimsByHeight(ctx, &types.QueryClaimsByHeightRequest{Height: 5})
	require.NotNil(t, err)
	require.Nil(t, resp)

	blockClaims := types.NewBlockClaims(10)
	claimItem := types.ClaimItem{
		From:      GetRandomBech32Addr().String(),
		To:        GetRandomBech32Addr().String(),
		Signature: "Signature",
		TxID:      GetRandomTxHash(),
		Message:   "HelloWorld",
		Signer:    GetRandomBech32Addr().String(),
	}
	blockClaims.Claims = append(blockClaims.Claims, claimItem)
	privKey := secp256k1.GenPrivKey()
	pubKey := privKey.PubKey()
	getBlockClaimsCall.Unset()
	mockKeeper.On("GetBlockClaims", mock.Anything, int64(5)).Return(blockClaims, nil)

	// when fail to sign , it should result in an error
	signCall := mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, errors.New("fail to sign"))
	resp, err = querier.ClaimsByHeight(ctx, &types.QueryClaimsByHeightRequest{Height: 5})
	require.NotNil(t, err)
	require.Nil(t, resp)
	signCall.Unset()

	signCall = mockSigner.On("Sign", mock.Anything, mock.Anything).Return([]byte{1, 2, 3, 4}, pubKey, nil)
	resp, err = querier.ClaimsByHeight(ctx, &types.QueryClaimsByHeightRequest{Height: 5})
	require.Nil(t, err)
	require.NotNil(t, resp)
	signCall.Unset()
}
