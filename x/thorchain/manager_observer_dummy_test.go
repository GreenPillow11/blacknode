package thorchain

import (
	"gitlab.com/blackprotocol/blacknode/common"
	cosmos "gitlab.com/blackprotocol/blacknode/common/cosmos"
	keeper "gitlab.com/blackprotocol/blacknode/x/thorchain/keeper"
)

type DummyObserverManager struct{}

func NewDummyObserverManager() *DummyObserverManager {
	return &DummyObserverManager{}
}

func (m *DummyObserverManager) BeginBlock()                                                  {}
func (m *DummyObserverManager) EndBlock(ctx cosmos.Context, keeper keeper.Keeper)            {}
func (m *DummyObserverManager) AppendObserver(chain common.Chain, addrs []cosmos.AccAddress) {}
func (m *DummyObserverManager) List() []cosmos.AccAddress                                    { return nil }
