package thorchain

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/blackprotocol/blacknode/x/thorchain/keeper"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

func Test_claimServer_Claim(t *testing.T) {
	ctx, mgr := GetManagersForTest()
	cs := NewClaimServerImpl(mgr.K, mgr)
	// nil msg should result in an error
	resp, err := cs.Claim(ctx, nil)
	assert.Nil(t, resp)
	assert.NotNil(t, err)

	// invalid MsgClaim should result in an error
	resp, err = cs.Claim(ctx, &types.MsgClaim{})
	assert.Nil(t, resp)
	assert.NotNil(t, err)

	// when failed to get observed TxIn Voter it should result in an error
	claimMsg := &types.MsgClaim{
		From:      GetRandomBech32Addr().String(),
		To:        GetRandomBech32Addr().String(),
		Signature: "signature",
		TxID:      GetRandomTxHash(),
		Message:   "hello world",
		Signer:    GetRandomBech32Addr().String(),
	}

	k, ok := mgr.K.(*keeper.MockKeeper)
	assert.True(t, ok)
	// when failed to get mimir , it should result in an error
	mockGetMimir := k.On("GetMimir", mock.Anything, mock.Anything).Return(1, errors.New("fail to get mimir"))
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetMimir.Unset()
	mockGetMimir = k.On("GetMimir", mock.Anything, mock.Anything).Return(1, nil)
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetMimir.Unset()

	k.On("GetMimir", mock.Anything, mock.Anything).Return(-1, nil)

	// when msg sender doesn't have enough fund to pay for native transaction fee , it should result in an error
	mockHasCoins := k.On("HasCoins", mock.Anything, mock.Anything, mock.Anything).Return(false)
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockHasCoins.Unset()
	k.On("HasCoins", mock.Anything, mock.Anything, mock.Anything).Return(true)

	// when fail to send native transaction fee to reserve , transaction should fail
	mockSendFromAccountToModule := k.On("SendFromAccountToModule", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("fail to charge native transaction fee"))
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockSendFromAccountToModule.Unset()
	k.On("SendFromAccountToModule", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	mockGetObservedTxInVoter := k.On("GetObservedTxInVoter", mock.Anything, claimMsg.TxID).Return(ObservedTxVoter{}, errors.New("fail to get observed Tx In"))
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetObservedTxInVoter.Unset()

	// when txin has been observed , it should result in an error
	mockGetObservedTxInVoter = k.On("GetObservedTxInVoter", mock.Anything, claimMsg.TxID).Return(ObservedTxVoter{
		TxID:   claimMsg.TxID,
		Tx:     GetRandomObservedTx(),
		Height: 10,
		Txs: []ObservedTx{
			GetRandomObservedTx(),
		},
		OutTxs:          nil,
		FinalisedHeight: 0,
		UpdatedVault:    false,
		Reverted:        false,
	}, nil)

	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetObservedTxInVoter.Unset()
	k.On("GetObservedTxInVoter", mock.Anything, claimMsg.TxID).Return(ObservedTxVoter{
		TxID: claimMsg.TxID,
	}, nil)

	// when tx_id has been observed in an outbound , it should result in an error
	mockGetObservedTxOutVoter := k.On("GetObservedTxOutVoter", mock.Anything, claimMsg.TxID).Return(ObservedTxVoter{
		TxID:   claimMsg.TxID,
		Tx:     GetRandomObservedTx(),
		Height: 10,
		Txs: []ObservedTx{
			GetRandomObservedTx(),
		},
		OutTxs:          nil,
		FinalisedHeight: 0,
		UpdatedVault:    false,
		Reverted:        false,
	}, nil)

	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetObservedTxOutVoter.Unset()

	k.On("GetObservedTxOutVoter", mock.Anything, claimMsg.TxID).Return(ObservedTxVoter{
		TxID: claimMsg.TxID,
	}, nil)

	// when blockClaim already exist
	mockGetBlockClaims := k.On("GetBlockClaims", mock.Anything, mock.Anything).Return(&types.BlockClaims{
		Height: 1,
		Claims: []types.ClaimItem{
			{
				From:      claimMsg.From,
				To:        claimMsg.To,
				Signature: claimMsg.Signature,
				TxID:      claimMsg.TxID,
				Message:   claimMsg.Message,
				Signer:    claimMsg.Signer,
			},
		},
	}, nil)
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockGetBlockClaims.Unset()
	k.On("GetBlockClaims", mock.Anything, mock.Anything).Return(&types.BlockClaims{
		Height: 1,
	}, nil)

	// when fail to AppendClaimItem , it should result in an error
	mockAppendClaimItem := k.On("AppendClaimItem", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("fail to append claim item"))
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, resp)
	assert.NotNil(t, err)
	mockAppendClaimItem.Unset()
	k.On("AppendClaimItem", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	resp, err = cs.Claim(ctx, claimMsg)
	assert.Nil(t, err)
	assert.NotNil(t, resp)
}
