package thorchain

import (
	"context"
	"errors"
	"fmt"

	"github.com/blang/semver"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/keeper"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

type claimServer struct {
	k   keeper.Keeper
	mgr *Mgrs
}

// NewClaimServerImpl create a new instance of msg server
func NewClaimServerImpl(keeper keeper.Keeper, mgr *Mgrs) types.MsgServer {
	return &claimServer{
		k:   keeper,
		mgr: mgr,
	}
}

// Claim process claim request
func (cs claimServer) Claim(c context.Context, mc *types.MsgClaim) (*types.MsgClaimResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	if mc == nil {
		return nil, errors.New("invalid claim message")
	}
	ctx.Logger().Info("receive MsgClaim", "from", mc.From,
		"to", mc.To,
		"tx_id", mc.TxID,
		"signature", mc.Signature,
		"msg", mc.Message)

	if err := mc.ValidateBasic(); err != nil {
		return nil, cosmos.ErrUnknownRequest("invalid claim message")
	}
	v := cs.mgr.GetVersion()
	switch {
	case v.GTE(semver.MustParse("0.1.0")):
		return cs.ClaimV1(ctx, mc)
	default:
		return nil, fmt.Errorf("invalid version(%s)", v.String())
	}
}

func (cs claimServer) ClaimV1(ctx cosmos.Context, mc *types.MsgClaim) (*types.MsgClaimResponse, error) {
	// when chain is halt , no one can claim anything
	haltHeight, err := cs.mgr.Keeper().GetMimir(ctx, "HaltTHORChain")
	if err != nil {
		return nil, fmt.Errorf("failed to get mimir setting: %w", err)
	}
	if haltHeight > 0 && ctx.BlockHeight() > haltHeight {
		return nil, fmt.Errorf("mimir has halted chain")
	}
	// charge transaction fee
	nativeTxFee, err := cs.mgr.Keeper().GetMimir(ctx, constants.NativeTransactionFee.String())
	if err != nil || nativeTxFee < 0 {
		nativeTxFee = cs.mgr.GetConstants().GetInt64Value(constants.NativeTransactionFee)
	}
	gas := common.NewCoin(common.RuneNative, cosmos.NewUint(uint64(nativeTxFee)))
	gasFee, err := gas.Native()
	if err != nil {
		return nil, fmt.Errorf("fail to get gas fee: %w", err)
	}

	totalCoins := cosmos.NewCoins(gasFee)
	if !cs.mgr.Keeper().HasCoins(ctx, mc.GetSigners()[0], totalCoins) {
		return nil, cosmos.ErrInsufficientCoins(err, "insufficient funds")
	}

	// send gas to reserve
	sdkErr := cs.mgr.Keeper().SendFromAccountToModule(ctx, mc.GetSigners()[0], ReserveName, common.NewCoins(gas))
	if sdkErr != nil {
		return nil, fmt.Errorf("unable to send gas to reserve: %w", sdkErr)
	}
	// make the claim tx has not been observed before
	observedTxIn, err := cs.k.GetObservedTxInVoter(ctx, mc.TxID)
	if err != nil {
		return nil, fmt.Errorf("fail to get observed tx in voter,err: %w", err)
	}
	if !observedTxIn.Tx.IsEmpty() {
		return nil, fmt.Errorf("%s has been observed", mc.TxID)
	}
	observedTxOut, err := cs.k.GetObservedTxOutVoter(ctx, mc.TxID)
	if err != nil {
		return nil, fmt.Errorf("fail to get observed tx in voter,err: %w", err)
	}
	if !observedTxOut.Tx.IsEmpty() {
		return nil, fmt.Errorf("%s has been observed", mc.TxID)
	}
	// make sure the same claim doesn't exist in the last 300 blocks
	signingTranPeriod := cs.mgr.GetConstants().GetInt64Value(constants.SigningTransactionPeriod)
	startHeight := ctx.BlockHeight() - signingTranPeriod
	if startHeight < 0 {
		startHeight = 0
	}

	for i := startHeight; i < ctx.BlockHeight(); i++ {
		blockClaim, err := cs.k.GetBlockClaims(ctx, i)
		if err != nil {
			ctx.Logger().Error("fail to get BlockClaim", "err", err, "height", i)
		}
		for _, item := range blockClaim.Claims {
			if item.TxID.Equals(mc.TxID) {
				return nil, fmt.Errorf("claim already exist on block height %d", i)
			}
		}
	}
	// save the claim to key value store
	if err := cs.k.AppendClaimItem(ctx, ctx.BlockHeight(), types.ClaimItem{
		From:      mc.From,
		To:        mc.To,
		Signature: mc.Signature,
		TxID:      mc.TxID,
		Message:   mc.Message,
		Signer:    mc.Signer,
	}); err != nil {
		return nil, fmt.Errorf("fail to get BlockClaims,err: %w", err)
	}
	return &types.MsgClaimResponse{}, nil
}

var _ types.MsgServer = claimServer{}
