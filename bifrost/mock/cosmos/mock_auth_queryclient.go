//nolint:forcetypeassert
package cosmos

import (
	"context"

	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	"github.com/stretchr/testify/mock"
	"google.golang.org/grpc"
)

type MockAuthQueryClient struct {
	mock.Mock
}

func (c *MockAuthQueryClient) Accounts(ctx context.Context, in *authtypes.QueryAccountsRequest, _ ...grpc.CallOption) (*authtypes.QueryAccountsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryAccountsResponse), args.Error(1)
}

// Account returns account details based on address.
func (c *MockAuthQueryClient) Account(ctx context.Context, in *authtypes.QueryAccountRequest, _ ...grpc.CallOption) (*authtypes.QueryAccountResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryAccountResponse), args.Error(1)
}

// AccountAddressByID returns account address based on account number.
func (c *MockAuthQueryClient) AccountAddressByID(ctx context.Context, in *authtypes.QueryAccountAddressByIDRequest, _ ...grpc.CallOption) (*authtypes.QueryAccountAddressByIDResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryAccountAddressByIDResponse), args.Error(1)
}

// Params queries all parameters.
func (c *MockAuthQueryClient) Params(ctx context.Context, in *authtypes.QueryParamsRequest, _ ...grpc.CallOption) (*authtypes.QueryParamsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryParamsResponse), args.Error(1)
}

// ModuleAccounts returns all the existing module accounts.
func (c *MockAuthQueryClient) ModuleAccounts(ctx context.Context, in *authtypes.QueryModuleAccountsRequest, _ ...grpc.CallOption) (*authtypes.QueryModuleAccountsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryModuleAccountsResponse), args.Error(1)
}

// ModuleAccountByName returns the module account info by module name
func (c *MockAuthQueryClient) ModuleAccountByName(ctx context.Context, in *authtypes.QueryModuleAccountByNameRequest, _ ...grpc.CallOption) (*authtypes.QueryModuleAccountByNameResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.QueryModuleAccountByNameResponse), args.Error(1)
}

// Bech32Prefix queries bech32Prefix
func (c *MockAuthQueryClient) Bech32Prefix(ctx context.Context, in *authtypes.Bech32PrefixRequest, _ ...grpc.CallOption) (*authtypes.Bech32PrefixResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.Bech32PrefixResponse), args.Error(1)
}

// AddressBytesToString converts Account Address bytes to string
func (c *MockAuthQueryClient) AddressBytesToString(ctx context.Context, in *authtypes.AddressBytesToStringRequest, _ ...grpc.CallOption) (*authtypes.AddressBytesToStringResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.AddressBytesToStringResponse), args.Error(1)
}

// AddressStringToBytes converts Address string to bytes
func (c *MockAuthQueryClient) AddressStringToBytes(ctx context.Context, in *authtypes.AddressStringToBytesRequest, _ ...grpc.CallOption) (*authtypes.AddressStringToBytesResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*authtypes.AddressStringToBytesResponse), args.Error(1)
}
