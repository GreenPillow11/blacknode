//nolint:forcetypeassert
package cosmos

import (
	"context"

	"github.com/stretchr/testify/mock"
	"google.golang.org/grpc"

	stypes "gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

type MockThorchainQueryClient struct {
	mock.Mock
}

func (c *MockThorchainQueryClient) InboundAddresses(ctx context.Context, in *stypes.QueryInboundAddressesRequest, _ ...grpc.CallOption) (*stypes.QueryInboundAddressesResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryInboundAddressesResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Pools(ctx context.Context, in *stypes.QueryPoolsRequest, _ ...grpc.CallOption) (*stypes.QueryPoolsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryPoolsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Pool(ctx context.Context, in *stypes.QueryPoolRequest, _ ...grpc.CallOption) (*stypes.QueryPoolResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryPoolResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) LiquidityProviders(ctx context.Context, in *stypes.QueryLiquidityProvidersRequest, _ ...grpc.CallOption) (*stypes.QueryLiquidityProvidersResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryLiquidityProvidersResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) LiquidityProvider(ctx context.Context, in *stypes.QueryLiquidityProviderRequest, _ ...grpc.CallOption) (*stypes.QueryLiquidityProviderResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryLiquidityProviderResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Saver(ctx context.Context, in *stypes.QuerySaverRequest, _ ...grpc.CallOption) (*stypes.QuerySaverResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QuerySaverResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Savers(ctx context.Context, in *stypes.QuerySaversRequest, _ ...grpc.CallOption) (*stypes.QuerySaversResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QuerySaversResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) TxVoters(ctx context.Context, in *stypes.QueryTxVoterRequest, _ ...grpc.CallOption) (*stypes.QueryTxVoterResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryTxVoterResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Tx(ctx context.Context, in *stypes.QueryTxRequest, _ ...grpc.CallOption) (*stypes.QueryTxResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryTxResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Keysigns(ctx context.Context, in *stypes.QueryKeysignsRequest, _ ...grpc.CallOption) (*stypes.QueryKeysignsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryKeysignsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) KeysignsByPubKey(ctx context.Context, in *stypes.QueryKeysignsByPubKeyRequest, _ ...grpc.CallOption) (*stypes.QueryKeysignsByPubKeyResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryKeysignsByPubKeyResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Keygens(ctx context.Context, in *stypes.QueryKeygensRequest, _ ...grpc.CallOption) (*stypes.QueryKeygensResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryKeygensResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) KeygensByPubKey(ctx context.Context, in *stypes.QueryKeygenByPubKeyRequest, _ ...grpc.CallOption) (*stypes.QueryKeygenByPubKeyResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryKeygenByPubKeyResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Queue(ctx context.Context, in *stypes.QueryQueueRequest, _ ...grpc.CallOption) (*stypes.QueryQueueResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryQueueResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Outbound(ctx context.Context, in *stypes.QueryOutboundRequest, _ ...grpc.CallOption) (*stypes.QueryOutboundResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryOutboundResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) ScheduledOutbound(ctx context.Context, in *stypes.QueryScheduledOutboundRequest, _ ...grpc.CallOption) (*stypes.QueryScheduledOutboundResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryScheduledOutboundResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Heights(ctx context.Context, in *stypes.QueryHeightsRequest, _ ...grpc.CallOption) (*stypes.QueryHeightsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryHeightsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) ChainHeights(ctx context.Context, in *stypes.QueryChainHeightsRequest, _ ...grpc.CallOption) (*stypes.QueryChainHeightsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryChainHeightsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Nodes(ctx context.Context, in *stypes.QueryNodesRequest, _ ...grpc.CallOption) (*stypes.QueryNodesResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryNodesResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Node(ctx context.Context, in *stypes.QueryNodeRequest, _ ...grpc.CallOption) (*stypes.QueryNodeResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryNodeResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Network(ctx context.Context, in *stypes.QueryNetworkRequest, _ ...grpc.CallOption) (*stypes.QueryNetworkResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryNetworkResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) POL(ctx context.Context, in *stypes.QueryPOLRequest, _ ...grpc.CallOption) (*stypes.QueryPOLResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryPOLResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) ModuleBalance(ctx context.Context, in *stypes.QueryModuleBalanceRequest, _ ...grpc.CallOption) (*stypes.QueryModuleBalanceResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryModuleBalanceResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Asgards(ctx context.Context, in *stypes.QueryAsgardsRequest, _ ...grpc.CallOption) (*stypes.QueryAsgardsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryAsgardsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Vault(ctx context.Context, in *stypes.QueryVaultRequest, _ ...grpc.CallOption) (*stypes.QueryVaultResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryVaultResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) VaultPubkeys(ctx context.Context, in *stypes.QueryVaultPubKeysRequest, _ ...grpc.CallOption) (*stypes.QueryVaultPubKeysResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryVaultPubKeysResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Constants(ctx context.Context, in *stypes.QueryConstantsRequest, _ ...grpc.CallOption) (*stypes.QueryConstantsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryConstantsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Mimirs(ctx context.Context, in *stypes.QueryMimirsRequest, _ ...grpc.CallOption) (*stypes.QueryMimirsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryMimirsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) MimirWithKey(ctx context.Context, in *stypes.QueryMimirWithKeyRequest, _ ...grpc.CallOption) (*stypes.QueryMimirWithKeyResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryMimirWithKeyResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) AdminMimirs(ctx context.Context, in *stypes.QueryAdminMimirRequest, _ ...grpc.CallOption) (*stypes.QueryAdminMimirResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryAdminMimirResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) NodesMimir(ctx context.Context, in *stypes.QueryNodeMimirRequest, _ ...grpc.CallOption) (*stypes.QueryNodeMimirResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryNodeMimirResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) NodeMimirWithAddress(ctx context.Context, in *stypes.QueryNodeMimirWithAddressRequest, _ ...grpc.CallOption) (*stypes.QueryNodeMimirWithAddressResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryNodeMimirWithAddressResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) AllNodeMimirs(ctx context.Context, in *stypes.QueryAllNodesMimirsRequest, _ ...grpc.CallOption) (*stypes.QueryAllNodesMimirsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryAllNodesMimirsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Ban(ctx context.Context, in *stypes.QueryBanRequest, _ ...grpc.CallOption) (*stypes.QueryBanResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryBanResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Ragnarok(ctx context.Context, in *stypes.QueryRagnarokRequest, _ ...grpc.CallOption) (*stypes.QueryRagnarokResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryRagnarokResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) KeygenMetric(ctx context.Context, in *stypes.QueryKeygenMetricRequest, _ ...grpc.CallOption) (*stypes.QueryKeygenMetricResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryKeygenMetricResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) TssMetric(ctx context.Context, in *stypes.QueryTssMetricRequest, _ ...grpc.CallOption) (*stypes.QueryTssMetricResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryTssMetricResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Thorname(ctx context.Context, in *stypes.QueryThornameRequest, _ ...grpc.CallOption) (*stypes.QueryThornameResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryThornameResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Version(ctx context.Context, in *stypes.QueryVersionRequest, _ ...grpc.CallOption) (*stypes.QueryVersionResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryVersionResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) Claims(ctx context.Context, in *stypes.QueryClaimsRequest, _ ...grpc.CallOption) (*stypes.QueryClaimsResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryClaimsResponse), args.Error(1)
}

func (c *MockThorchainQueryClient) ClaimsByHeight(ctx context.Context, in *stypes.QueryClaimsByHeightRequest, _ ...grpc.CallOption) (*stypes.QueryClaimsByHeightResponse, error) {
	args := c.Called(ctx, in)
	return args.Get(0).(*stypes.QueryClaimsByHeightResponse), args.Error(1)
}
