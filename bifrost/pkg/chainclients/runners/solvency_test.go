package runners

import (
	"sync"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/crypto/hd"
	ckeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	"github.com/cosmos/cosmos-sdk/simapp"
	"github.com/stretchr/testify/mock"
	. "gopkg.in/check.v1"

	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	cosmos2 "gitlab.com/blackprotocol/blacknode/bifrost/mock/cosmos"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/cmd"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

func TestPackage(t *testing.T) { TestingT(t) }

type SolvencyTestSuite struct {
	sp   *DummySolvencyCheckProvider
	m    *metrics.Metrics
	cfg  config.BifrostClientConfiguration
	keys *thorclient.Keys
}

var _ = Suite(&SolvencyTestSuite{})

func (s *SolvencyTestSuite) SetUpSuite(c *C) {
	sp := &DummySolvencyCheckProvider{}
	s.sp = sp

	m, _ := metrics.NewMetrics(config.BifrostMetricsConfiguration{
		Enabled:      false,
		ListenPort:   9090,
		ReadTimeout:  time.Second,
		WriteTimeout: time.Second,
		Chains:       common.Chains{common.BNBChain},
	})
	s.m = m

	cfg := config.BifrostClientConfiguration{
		ChainID:         "thorchain",
		ChainHost:       "localhost",
		SignerName:      "bob",
		SignerPasswd:    "password",
		ChainHomeFolder: ".",
	}
	cdc := simapp.MakeTestEncodingConfig().Codec
	kb := ckeys.NewInMemory(cdc)
	_, _, err := kb.NewMnemonic(cfg.SignerName, ckeys.English, cmd.THORChainHDPath, cfg.SignerPasswd, hd.Secp256k1)
	c.Assert(err, IsNil)
	s.cfg = cfg
	s.keys = thorclient.NewKeysWithKeybase(kb, cfg.SignerName, cfg.SignerPasswd)

	c.Assert(err, IsNil)
}

func (s *SolvencyTestSuite) TestSolvencyCheck(c *C) {
	bridge, err := thorclient.NewThorchainBridge(config.BifrostClientConfiguration{
		ChainID:         "thorchain",
		SignerName:      "bob",
		SignerPasswd:    "password",
		ChainHomeFolder: ".",
		ChainHost:       "127.0.0.1:1317",
		ChainGRPC:       "localhost:9090",
	}, s.m, s.keys)
	c.Assert(err, IsNil)
	mockThorchainClient := new(cosmos2.MockThorchainQueryClient)
	bridge.SetQueryClient(mockThorchainClient)
	mockHaltBNBChain := mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "HaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 0}, nil)
	mockSolvencyHalt := mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "SolvencyHaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 0}, nil)
	stopchan := make(chan struct{})
	wg := &sync.WaitGroup{}

	// Happy path, shouldn't check solvency if nothing halted (chain clients will report solvency)
	s.sp.ResetChecks()
	wg.Add(1)
	go SolvencyCheckRunner(common.BNBChain, s.sp, bridge, stopchan, wg, time.Millisecond*500)
	time.Sleep(time.Second)

	c.Assert(s.sp.ShouldReportSolvencyRan, Equals, false)
	c.Assert(s.sp.ReportSolvencyRun, Equals, false)

	// Admin halted, still don't check solvency
	mockHaltBNBChain.Unset()
	mockHaltBNBChain = mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "HaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 1}, nil)

	s.sp.ResetChecks()
	// go SolvencyCheckRunner(common.BNBChain, s.sp, bridge, stopchan, wg, constants.ThorchainBlockTime)
	time.Sleep(time.Second)

	c.Assert(s.sp.ShouldReportSolvencyRan, Equals, false)
	c.Assert(s.sp.ReportSolvencyRun, Equals, false)

	// Double-spend check halted chain client, check solvency here
	mockHaltBNBChain.Unset()
	mockHaltBNBChain = mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "HaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 10}, nil)

	s.sp.ResetChecks()
	// go SolvencyCheckRunner(common.BNBChain, s.sp, bridge, stopchan, wg, constants.ThorchainBlockTime)
	time.Sleep(time.Second)

	c.Assert(s.sp.ShouldReportSolvencyRan, Equals, true)
	c.Assert(s.sp.ReportSolvencyRun, Equals, true)
	mockHaltBNBChain.Unset()
	mockHaltBNBChain = mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "HaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 0}, nil)

	// Solvency halted chain, need to report solvency here as chain client is paused
	mockSolvencyHalt.Unset()
	mockSolvencyHalt = mockThorchainClient.On("MimirWithKey", mock.Anything, &types.QueryMimirWithKeyRequest{Name: "SolvencyHaltBNBChain"}).Return(&types.QueryMimirWithKeyResponse{Value: 10}, nil)

	s.sp.ResetChecks()
	// go SolvencyCheckRunner(common.BNBChain, s.sp, bridge, stopchan, wg, constants.ThorchainBlockTime)
	time.Sleep(time.Second)

	c.Assert(s.sp.ShouldReportSolvencyRan, Equals, true)
	c.Assert(s.sp.ReportSolvencyRun, Equals, true)
	close(stopchan)
	wg.Wait()
	mockHaltBNBChain.Unset()
	mockSolvencyHalt.Unset()
}

// Mock SolvencyCheckProvider
type DummySolvencyCheckProvider struct {
	ShouldReportSolvencyRan bool
	ReportSolvencyRun       bool
}

func (d *DummySolvencyCheckProvider) ResetChecks() {
	d.ShouldReportSolvencyRan = false
	d.ReportSolvencyRun = false
}

func (d *DummySolvencyCheckProvider) GetHeight() (int64, error) {
	return 0, nil
}

func (d *DummySolvencyCheckProvider) ShouldReportSolvency(_ int64) bool {
	d.ShouldReportSolvencyRan = true
	return true
}

func (d *DummySolvencyCheckProvider) ReportSolvency(_ int64) error {
	d.ReportSolvencyRun = true
	return nil
}

func (s *SolvencyTestSuite) TestIsVaultSolvent(c *C) {
	vault := types.Vault{
		BlockHeight: 1,
		PubKey:      types.GetRandomPubKey(),
		Coins: common.NewCoins(
			common.NewCoin(common.ETHAsset, cosmos.NewUint(102400000000)),
		),
		Type:   types.VaultType_AsgardVault,
		Status: types.VaultStatus_ActiveVault,
	}
	acct := common.Account{
		Sequence:      0,
		AccountNumber: 0,
		Coins:         common.NewCoins(common.NewCoin(common.ETHAsset, cosmos.NewUint(102400000000))),
	}
	c.Assert(IsVaultSolvent(acct, vault, cosmos.NewUint(0)), Equals, true)
	acct = common.Account{
		Sequence:      0,
		AccountNumber: 0,
		Coins:         common.NewCoins(common.NewCoin(common.ETHAsset, cosmos.NewUint(102305000000))),
	}
	c.Assert(IsVaultSolvent(acct, vault, cosmos.NewUint(80000*120)), Equals, true)
	acct = common.Account{
		Sequence:      0,
		AccountNumber: 0,
		Coins:         common.NewCoins(common.NewCoin(common.ETHAsset, cosmos.NewUint(102205000000))),
	}
	c.Assert(IsVaultSolvent(acct, vault, cosmos.NewUint(80000*120)), Equals, false)
}
