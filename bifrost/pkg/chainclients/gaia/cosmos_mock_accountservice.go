package gaia

import (
	"context"
	"fmt"

	atypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	"google.golang.org/grpc"
)

// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MockAccountServiceClient interface {
	// Accounts returns all the existing accounts
	//
	// Since: cosmos-sdk 0.43
	Accounts(ctx context.Context, in *atypes.QueryAccountsRequest, opts ...grpc.CallOption) (*atypes.QueryAccountsResponse, error)
	// Account returns account details based on address.
	Account(ctx context.Context, in *atypes.QueryAccountRequest, opts ...grpc.CallOption) (*atypes.QueryAccountResponse, error)
	// Params queries all parameters.
	Params(ctx context.Context, in *atypes.QueryParamsRequest, opts ...grpc.CallOption) (*atypes.QueryParamsResponse, error)
	AccountAddressByID(ctx context.Context, in *atypes.QueryAccountAddressByIDRequest, opts ...grpc.CallOption) (*atypes.QueryAccountAddressByIDResponse, error)
	ModuleAccounts(ctx context.Context, in *atypes.QueryModuleAccountsRequest, opts ...grpc.CallOption) (*atypes.QueryModuleAccountsResponse, error)
	ModuleAccountByName(ctx context.Context, in *atypes.QueryModuleAccountByNameRequest, opts ...grpc.CallOption) (*atypes.QueryModuleAccountByNameResponse, error)
	Bech32Prefix(ctx context.Context, in *atypes.Bech32PrefixRequest, opts ...grpc.CallOption) (*atypes.Bech32PrefixResponse, error)
	AddressBytesToString(ctx context.Context, in *atypes.AddressBytesToStringRequest, opts ...grpc.CallOption) (*atypes.AddressBytesToStringResponse, error)
	AddressStringToBytes(ctx context.Context, in *atypes.AddressStringToBytesRequest, opts ...grpc.CallOption) (*atypes.AddressStringToBytesResponse, error)
}

type mockAccountServiceClient struct{}

func NewMockAccountServiceClient() MockAccountServiceClient {
	return &mockAccountServiceClient{}
}

func (c *mockAccountServiceClient) Account(ctx context.Context, in *atypes.QueryAccountRequest, opts ...grpc.CallOption) (*atypes.QueryAccountResponse, error) {
	out := new(atypes.QueryAccountResponse)
	err := unmarshalJSONToPb("./test-data/account_by_address.json", out)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal block by height: %s", err)
	}
	return out, nil
}

func (c *mockAccountServiceClient) Accounts(ctx context.Context, in *atypes.QueryAccountsRequest, opts ...grpc.CallOption) (*atypes.QueryAccountsResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) Params(ctx context.Context, in *atypes.QueryParamsRequest, opts ...grpc.CallOption) (*atypes.QueryParamsResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) AccountAddressByID(ctx context.Context, in *atypes.QueryAccountAddressByIDRequest, opts ...grpc.CallOption) (*atypes.QueryAccountAddressByIDResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) ModuleAccounts(ctx context.Context, in *atypes.QueryModuleAccountsRequest, opts ...grpc.CallOption) (*atypes.QueryModuleAccountsResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) ModuleAccountByName(ctx context.Context, in *atypes.QueryModuleAccountByNameRequest, opts ...grpc.CallOption) (*atypes.QueryModuleAccountByNameResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) Bech32Prefix(ctx context.Context, in *atypes.Bech32PrefixRequest, opts ...grpc.CallOption) (*atypes.Bech32PrefixResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) AddressBytesToString(ctx context.Context, in *atypes.AddressBytesToStringRequest, opts ...grpc.CallOption) (*atypes.AddressBytesToStringResponse, error) {
	return nil, nil
}

func (c *mockAccountServiceClient) AddressStringToBytes(ctx context.Context, in *atypes.AddressStringToBytesRequest, opts ...grpc.CallOption) (*atypes.AddressStringToBytesResponse, error) {
	return nil, nil
}
