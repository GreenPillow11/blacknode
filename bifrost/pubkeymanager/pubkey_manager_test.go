package pubkeymanager

import (
	"testing"

	"github.com/stretchr/testify/mock"
	. "gopkg.in/check.v1"

	cosmos2 "gitlab.com/blackprotocol/blacknode/bifrost/mock/cosmos"
	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/config"
	stypes "gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

func Test(t *testing.T) { TestingT(t) }

type PubKeyMgrSuite struct{}

var _ = Suite(&PubKeyMgrSuite{})

func (s *PubKeyMgrSuite) TestPubkeyMgr(c *C) {
	pk1 := stypes.GetRandomPubKey()
	pk2 := stypes.GetRandomPubKey()
	pk3 := stypes.GetRandomPubKey()
	pk4 := stypes.GetRandomPubKey()

	pubkeyMgr, err := NewPubKeyManager(nil, nil)
	c.Assert(err, IsNil)
	c.Check(pubkeyMgr.HasPubKey(pk1), Equals, false)
	pubkeyMgr.AddPubKey(pk1, true)
	c.Check(pubkeyMgr.HasPubKey(pk1), Equals, true)
	c.Check(pubkeyMgr.pubkeys[0].PubKey.Equals(pk1), Equals, true)
	c.Check(pubkeyMgr.pubkeys[0].Signer, Equals, true)

	pubkeyMgr.AddPubKey(pk2, false)
	c.Check(pubkeyMgr.HasPubKey(pk2), Equals, true)
	c.Check(pubkeyMgr.pubkeys[1].PubKey.Equals(pk2), Equals, true)
	c.Check(pubkeyMgr.pubkeys[1].Signer, Equals, false)

	pks := pubkeyMgr.GetPubKeys()
	c.Assert(pks, HasLen, 2)

	pks = pubkeyMgr.GetSignPubKeys()
	c.Assert(pks, HasLen, 1)
	c.Check(pks[0].Equals(pk1), Equals, true)

	// remove a pubkey
	pubkeyMgr.RemovePubKey(pk2)
	c.Check(pubkeyMgr.HasPubKey(pk2), Equals, false)

	addr, err := pk1.GetAddress(common.LTCChain)
	c.Assert(err, IsNil)
	ok, _ := pubkeyMgr.IsValidPoolAddress(addr.String(), common.LTCChain)
	c.Assert(ok, Equals, true)

	addr, err = pk3.GetAddress(common.LTCChain)
	c.Assert(err, IsNil)
	pubkeyMgr.AddNodePubKey(pk4)
	c.Check(pubkeyMgr.GetNodePubKey().String(), Equals, pk4.String())
	ok, _ = pubkeyMgr.IsValidPoolAddress(addr.String(), common.LTCChain)
	c.Assert(ok, Equals, false)
}

func (s *PubKeyMgrSuite) TestFetchKeys(c *C) {
	pk1 := stypes.GetRandomPubKey()
	pk2 := stypes.GetRandomPubKey()
	pk3 := stypes.GetRandomPubKey()

	cfg := config.BifrostClientConfiguration{
		ChainID:   "thorchain",
		ChainHost: "localhost",
		ChainGRPC: "localhost:9090",
	}
	bridge, err := thorclient.NewThorchainBridge(cfg, nil, nil)
	c.Assert(err, IsNil)
	mockThorchainClient := new(cosmos2.MockThorchainQueryClient)
	// mock when the bridge call into `VaultPubkeys` , return this result
	mockVaultPubKeys := mockThorchainClient.On("VaultPubkeys", mock.Anything, mock.Anything).Return(&stypes.QueryVaultPubKeysResponse{
		Asgards: []*stypes.QueryVaultPubKeyContract{
			{
				PubKey: pk1.String(),
				Routers: []stypes.ChainContract{
					{
						Chain:  common.ETHChain,
						Router: "0xE65e9d372F8cAcc7b6dfcd4af6507851Ed31bb44",
					},
				},
			},
		},
	}, nil)
	mockGetAsgards := mockThorchainClient.On("Asgards", mock.Anything, mock.Anything).Return(&stypes.QueryAsgardsResponse{
		Vaults: []*stypes.QueryVault{
			{
				BlockHeight: 1024,
				PubKey:      stypes.GetRandomPubKey(),
				Coins:       nil,
				Type:        stypes.VaultType_AsgardVault,
				Status:      stypes.VaultStatus_ActiveVault,
				StatusSince: 1024,
				Membership: []string{
					pk3.String(),
				},
				Chains: []string{
					common.BTCChain.String(),
					common.ETHChain.String(),
				},
				InboundTxCount:        0,
				OutboundTxCount:       0,
				PendingTxBlockHeights: nil,
				Routers: []stypes.ChainContract{
					{
						Chain:  common.ETHChain,
						Router: "0xE65e9d372F8cAcc7b6dfcd4af6507851Ed31bb44",
					},
				},
				Addresses: nil,
			},
		},
	}, nil)
	bridge.SetQueryClient(mockThorchainClient)
	pubkeyMgr, err := NewPubKeyManager(bridge, nil)
	c.Assert(err, IsNil)
	hasCallbackFired := false
	callBack := func(pk common.PubKey) error {
		hasCallbackFired = true
		return nil
	}
	pubkeyMgr.RegisterCallback(callBack)
	pubkeyMgr.AddPubKey(pk2, false)
	c.Check(hasCallbackFired, Equals, true)

	// add a key that is the node account, ensure it doesn't get removed
	pubkeyMgr.pubkeys = append(pubkeyMgr.pubkeys, pubKeyInfo{
		PubKey:      pk3,
		Signer:      true,
		NodeAccount: true,
		Contracts:   map[common.Chain]common.Address{},
	})
	c.Check(len(pubkeyMgr.GetPubKeys()), Equals, 2)
	err = pubkeyMgr.Start()
	c.Assert(err, IsNil)
	pubkeyMgr.fetchPubKeys()
	pubKeys := pubkeyMgr.GetPubKeys()
	c.Check(len(pubKeys), Equals, 3)
	c.Check(pubKeys[0].Equals(pk1), Equals, false)
	c.Check(pubKeys[1].Equals(pk3), Equals, true)
	c.Check(pubkeyMgr.pubkeys[1].Signer, Equals, true)
	err = pubkeyMgr.Stop()
	c.Assert(err, IsNil)
	mockVaultPubKeys.Unset()
	mockGetAsgards.Unset()
}
