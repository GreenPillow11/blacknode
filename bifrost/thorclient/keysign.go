package thorclient

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/blackprotocol/blacknode/bifrost/thorclient/types"
	stypes "gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

var ErrNotFound = fmt.Errorf("not found")

type QueryKeysign struct {
	Keysign   types.TxOut `json:"keysign"`
	Signature string      `json:"signature"`
}

// GetKeysign retrieves txout from this block height from thorchain
func (b *ThorchainBridge) GetKeysign(blockHeight int64, pk string) (types.TxOut, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.KeysignsByPubKey(ctx, &stypes.QueryKeysignsByPubKeyRequest{
		Height: blockHeight,
		PubKey: pk,
	})
	if err != nil {
		return types.TxOut{}, fmt.Errorf("failed to get tx from a block height: %w", err)
	}
	if resp.Signature == "" {
		return types.TxOut{}, errors.New("invalid keysign signature: empty")
	}
	// it is empty , so return it
	if len(resp.Keysign.TxArray) == 0 {
		return types.TxOut{
			Height:  blockHeight,
			TxArray: nil,
		}, nil
	}
	buf, err := json.Marshal(resp.Keysign)
	if err != nil {
		return types.TxOut{}, fmt.Errorf("fail to marshal keysign block to json: %w", err)
	}
	pubKey, err := b.keys.GetSignerInfo().GetPubKey()
	if err != nil {
		return types.TxOut{}, fmt.Errorf("fail to get pubkey,err: %w", err)
	}
	s, err := base64.StdEncoding.DecodeString(resp.Signature)
	if err != nil {
		return types.TxOut{}, errors.New("invalid keysign signature: cannot decode signature")
	}
	if !pubKey.VerifySignature(buf, s) {
		return types.TxOut{}, errors.New("invalid keysign signature: bad signature")
	}

	// ensure the block height received is the one requested. Without this
	// check, an attacker could use a replay attack to steal funds
	if resp.Keysign.Height != blockHeight {
		return types.TxOut{}, fmt.Errorf("invalid keysign: block height mismatch (%d vs %d)", resp.Keysign.Height, blockHeight)
	}

	out := types.TxOut{
		Height: resp.Keysign.Height,
	}
	for _, item := range resp.Keysign.TxArray {
		out.TxArray = append(out.TxArray, types.TxArrayItem{
			Chain:                 item.Chain,
			ToAddress:             item.ToAddress,
			VaultPubKey:           item.VaultPubKey,
			Coin:                  item.Coin,
			Memo:                  item.Memo,
			MaxGas:                item.MaxGas,
			GasRate:               item.GasRate,
			InHash:                item.InHash,
			OutHash:               item.OutHash,
			Aggregator:            item.Aggregator,
			AggregatorTargetAsset: item.AggregatorTargetAsset,
			AggregatorTargetLimit: item.AggregatorTargetLimit,
		})
	}
	return out, nil
}
