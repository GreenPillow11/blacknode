package thorclient

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

// GetKeygenBlock retrieves keygen request for the given block height from thorchain
func (b *ThorchainBridge) GetKeygenBlock(blockHeight int64, pk string) (types.KeygenBlock, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.KeygensByPubKey(ctx, &types.QueryKeygenByPubKeyRequest{
		Height: blockHeight,
		PubKey: pk,
	})
	if err != nil {
		return types.KeygenBlock{}, fmt.Errorf("failed to get keygen for a block height: %w", err)
	}
	if resp.Signature == "" {
		return types.KeygenBlock{}, errors.New("invalid keygen signature: empty")
	}
	if resp.KeygenBlock == nil {
		return types.KeygenBlock{}, nil
	}
	if len(resp.KeygenBlock.Keygens) == 0 {
		return types.KeygenBlock{
			Height:  resp.KeygenBlock.Height,
			Keygens: nil,
		}, nil
	}
	buf, err := json.Marshal(resp.KeygenBlock)
	if err != nil {
		return types.KeygenBlock{}, fmt.Errorf("fail to marshal keygen block to json: %w", err)
	}
	pubKey, err := b.keys.GetSignerInfo().GetPubKey()
	if err != nil {
		return types.KeygenBlock{}, fmt.Errorf("fail to get pubkey,err: %w", err)
	}
	s, err := base64.StdEncoding.DecodeString(resp.Signature)
	if err != nil {
		return types.KeygenBlock{}, errors.New("invalid keygen signature: cannot decode signature")
	}
	if !pubKey.VerifySignature(buf, s) {
		return types.KeygenBlock{}, errors.New("invalid keygen signature: bad signature")
	}
	return *resp.KeygenBlock, nil
}
