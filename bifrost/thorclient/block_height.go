package thorclient

import (
	"fmt"
	"time"

	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/constants"
	"gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

// GetLastObservedInHeight returns the lastobservedin value for the chain past in
func (b *ThorchainBridge) GetLastObservedInHeight(chain common.Chain) (int64, error) {
	lastblock, err := b.getLastBlock(chain)
	if err != nil {
		return 0, fmt.Errorf("failed to GetLastObservedInHeight: %w", err)
	}
	return lastblock.LastObservedIn, nil
}

// GetLastSignedOutHeight returns the lastsignedout value for thorchain
func (b *ThorchainBridge) GetLastSignedOutHeight(chain common.Chain) (int64, error) {
	lastblock, err := b.getLastBlock(chain)
	if err != nil {
		return 0, fmt.Errorf("failed to GetLastSignedOutHeight: %w", err)
	}
	return lastblock.LastSignedOut, nil
}

// GetBlockHeight returns the current height for thorchain blocks
func (b *ThorchainBridge) GetBlockHeight() (int64, error) {
	if time.Since(b.lastBlockHeightCheck) < constants.ThorchainBlockTime && b.lastThorchainBlockHeight > 0 {
		return b.lastThorchainBlockHeight, nil
	}
	latestBlocks, err := b.getHeights()
	if err != nil {
		return 0, fmt.Errorf("failed to GetThorchainHeight: %w", err)
	}
	b.lastBlockHeightCheck = time.Now()
	for _, item := range latestBlocks {
		b.lastThorchainBlockHeight = item.Thorchain
		return item.Thorchain, nil
	}
	return 0, fmt.Errorf("failed to GetThorchainHeight")
}

// getLastBlock calls the /lastblock/{chain} endpoint and Unmarshal's into the QueryResLastBlockHeights type
func (b *ThorchainBridge) getLastBlock(chain common.Chain) (*types.QueryLastBlockHeight, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.ChainHeights(ctx, &types.QueryChainHeightsRequest{Chain: chain.String()})
	if err != nil {
		return nil, fmt.Errorf("failed to get lastblock: %w", err)
	}
	return resp.BlockHeight, nil
}

func (b *ThorchainBridge) getHeights() ([]*types.QueryLastBlockHeight, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Heights(ctx, &types.QueryHeightsRequest{})
	if err != nil {
		return nil, fmt.Errorf("failed to get lastblock: %w", err)
	}
	return resp.BlockHeights, nil
}
