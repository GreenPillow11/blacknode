package thorclient

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/blang/semver"
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	banktypes "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	rpchttp "github.com/tendermint/tendermint/rpc/client/http"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/blackprotocol/blacknode/app"
	"gitlab.com/blackprotocol/blacknode/bifrost/metrics"
	"gitlab.com/blackprotocol/blacknode/common"
	"gitlab.com/blackprotocol/blacknode/common/cosmos"
	"gitlab.com/blackprotocol/blacknode/config"
	"gitlab.com/blackprotocol/blacknode/constants"
	stypes "gitlab.com/blackprotocol/blacknode/x/thorchain/types"
)

const (
	StatusEndpoint = "/status"
)

// ThorchainBridge will be used to send tx to THORChain
type ThorchainBridge struct {
	logger                   zerolog.Logger
	cfg                      config.BifrostClientConfiguration
	keys                     *Keys
	errCounter               *prometheus.CounterVec
	m                        *metrics.Metrics
	blockHeight              int64
	accountNumber            uint64
	seqNumber                uint64
	qc                       stypes.QueryClient
	grpcConn                 *grpc.ClientConn
	broadcastLock            *sync.RWMutex
	authQueryClient          authtypes.QueryClient
	lastBlockHeightCheck     time.Time
	lastThorchainBlockHeight int64
}

// NewThorchainBridge create a new instance of ThorchainBridge
func NewThorchainBridge(cfg config.BifrostClientConfiguration, m *metrics.Metrics, k *Keys) (*ThorchainBridge, error) {
	// main module logger
	logger := log.With().Str("module", "thorchain_client").Logger()

	if len(cfg.ChainID) == 0 {
		return nil, errors.New("chain id is empty")
	}

	if len(cfg.ChainHost) == 0 {
		return nil, errors.New("chain host is empty")
	}
	if len(cfg.ChainGRPC) == 0 {
		return nil, errors.New("chain grpc is empty")
	}
	// cfg.ChainHost will be point to grpc
	// connect to GRPC endpoint
	grpcConn, err := grpc.Dial(cfg.ChainGRPC, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("fail to connect to GRPC endpoint(%s),err: %w", cfg.ChainHost, err)
	}

	return &ThorchainBridge{
		logger:          logger,
		cfg:             cfg,
		keys:            k,
		errCounter:      m.GetCounterVec(metrics.ThorchainClientError),
		qc:              stypes.NewQueryClient(grpcConn),
		authQueryClient: authtypes.NewQueryClient(grpcConn),
		m:               m,
		grpcConn:        grpcConn,
		broadcastLock:   &sync.RWMutex{},
	}, nil
}

// MakeLegacyCodec creates codec
func MakeLegacyCodec() *codec.LegacyAmino {
	cdc := codec.NewLegacyAmino()
	banktypes.RegisterLegacyAminoCodec(cdc)
	authtypes.RegisterLegacyAminoCodec(cdc)
	cosmos.RegisterCodec(cdc)
	stypes.RegisterCodec(cdc)
	return cdc
}

// GetContext return a valid context with all relevant values set
func (b *ThorchainBridge) GetContext() client.Context {
	ctx := client.Context{}
	ctx = ctx.WithKeyring(b.keys.GetKeybase())
	ctx = ctx.WithChainID(string(b.cfg.ChainID))
	ctx = ctx.WithHomeDir(b.cfg.ChainHomeFolder)
	ctx = ctx.WithFromName(b.cfg.SignerName)
	fromAddr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		panic(err)
	}
	ctx = ctx.WithFromAddress(fromAddr)
	ctx = ctx.WithBroadcastMode("sync")

	encodingConfig := app.MakeEncodingConfig()
	ctx = ctx.WithCodec(encodingConfig.Marshaler)
	ctx = ctx.WithInterfaceRegistry(encodingConfig.InterfaceRegistry)
	ctx = ctx.WithTxConfig(encodingConfig.TxConfig)
	ctx = ctx.WithLegacyAmino(encodingConfig.Amino)
	ctx = ctx.WithAccountRetriever(authtypes.AccountRetriever{})

	remote := b.cfg.ChainRPC
	if !strings.HasSuffix(b.cfg.ChainHost, "http") {
		remote = fmt.Sprintf("tcp://%s", remote)
	}
	ctx = ctx.WithNodeURI(remote)
	socketClient, err := rpchttp.New(remote, "/websocket")
	if err != nil {
		panic(err)
	}
	ctx = ctx.WithClient(socketClient)
	return ctx
}

// getAccountNumberAndSequenceNumber returns account and Sequence number required to post into thorchain
func (b *ThorchainBridge) getAccountNumberAndSequenceNumber() (uint64, uint64, error) {
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return 0, 0, fmt.Errorf("fail to get address: %w", err)
	}

	resp, err := b.authQueryClient.Account(context.Background(), &authtypes.QueryAccountRequest{
		Address: addr.String(),
	})
	if err != nil {
		return 0, 0, fmt.Errorf("fail to get auth accounts,err: %w", err)
	}
	ba := new(authtypes.BaseAccount)
	err = ba.Unmarshal(resp.Account.GetValue())
	if err != nil {
		return 0, 0, fmt.Errorf("failed to decode account,err: %w", err)
	}
	return ba.AccountNumber, ba.Sequence, nil
}

// GetConfig return the configuration
func (b *ThorchainBridge) GetConfig() config.BifrostClientConfiguration {
	return b.cfg
}

// PostKeysignFailure generate and  post a keysign fail tx to thorchan
func (b *ThorchainBridge) PostKeysignFailure(blame stypes.Blame, height int64, memo string, coins common.Coins, pubkey common.PubKey) (common.TxID, error) {
	start := time.Now()
	defer func() {
		b.m.GetHistograms(metrics.SignToThorchainDuration).Observe(time.Since(start).Seconds())
	}()
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return common.BlankTxID, fmt.Errorf("fail to get address: %w", err)
	}
	msg, err := stypes.NewMsgTssKeysignFail(height, blame, memo, coins, addr, pubkey)
	if err != nil {
		return common.BlankTxID, fmt.Errorf("fail to create keysign fail message: %w", err)
	}
	return b.Broadcast(msg)
}

// GetErrataMsg get errata tx from params
func (b *ThorchainBridge) GetErrataMsg(txID common.TxID, chain common.Chain) sdk.Msg {
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		b.logger.Err(err).Msg("fail to get address")
		return nil
	}
	return stypes.NewMsgErrataTx(txID, chain, addr)
}

// GetSolvencyMsg create MsgSolvency from the given parameters
func (b *ThorchainBridge) GetSolvencyMsg(height int64, chain common.Chain, pubKey common.PubKey, coins common.Coins) sdk.Msg {
	// To prevent different MsgSolvency ID incompatibility between nodes with different coin-observation histories,
	// only report coins for which the amounts are not currently 0.
	coins = coins.NoneEmpty()
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		b.logger.Err(err).Msg("fail to get address")
		return nil
	}
	msg, err := stypes.NewMsgSolvency(chain, pubKey, coins, height, addr)
	if err != nil {
		b.logger.Err(err).Msg("fail to create MsgSolvency")
		return nil
	}
	return msg
}

// GetKeygenStdTx get keygen tx from params
func (b *ThorchainBridge) GetKeygenStdTx(poolPubKey common.PubKey, blame stypes.Blame, inputPks common.PubKeys, keygenType stypes.KeygenType, chains common.Chains, height, keygenTime int64) (sdk.Msg, error) {
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return nil, fmt.Errorf("fail to get address,err: %w", err)
	}
	return stypes.NewMsgTssPool(inputPks.Strings(), poolPubKey, keygenType, height, blame, chains.Strings(), addr, keygenTime)
}

// GetObservationsStdTx get observations tx from txIns
func (b *ThorchainBridge) GetObservationsStdTx(txIns stypes.ObservedTxs) ([]cosmos.Msg, error) {
	if len(txIns) == 0 {
		return nil, nil
	}
	inbound := stypes.ObservedTxs{}
	outbound := stypes.ObservedTxs{}

	// spilt our txs into inbound vs outbound txs
	for _, tx := range txIns {
		chain := common.BTCChain
		if len(tx.Tx.Coins) > 0 {
			chain = tx.Tx.Coins[0].Asset.Chain
		}

		obAddr, err := tx.ObservedPubKey.GetAddress(chain)
		if err != nil {
			return nil, err
		}
		// for consolidate UTXO tx, both From & To address will be the asgard address
		// thus here we need to make sure that one add to inbound , the other add to outbound
		if tx.Tx.ToAddress.Equals(obAddr) && !inbound.Contains(tx) { // nolint
			inbound = append(inbound, tx)
		} else if tx.Tx.FromAddress.Equals(obAddr) && !outbound.Contains(tx) {
			// for outbound transaction , there is no need to do confirmation counting
			tx.FinaliseHeight = tx.BlockHeight
			outbound = append(outbound, tx)
		} else {
			return nil, errors.New("could not determine if this tx as inbound or outbound")
		}
	}
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return nil, fmt.Errorf("fail to get address,err: %w", err)
	}
	var msgs []cosmos.Msg
	if len(inbound) > 0 {
		msgs = append(msgs, stypes.NewMsgObservedTxIn(inbound, addr))
	}
	if len(outbound) > 0 {
		msgs = append(msgs, stypes.NewMsgObservedTxOut(outbound, addr))
	}

	return msgs, nil
}

// EnsureNodeWhitelistedWithTimeout check node is whitelisted with timeout retry
func (b *ThorchainBridge) EnsureNodeWhitelistedWithTimeout() error {
	for {
		select {
		case <-time.After(time.Hour):
			return errors.New("observer is not whitelisted yet")
		default:
			err := b.EnsureNodeWhitelisted()
			if err == nil {
				// node had been whitelisted
				return nil
			}
			b.logger.Error().Err(err).Msg("observer is not whitelisted , will retry a bit later")
			time.Sleep(time.Second * 30)
		}
	}
}

// EnsureNodeWhitelisted will call to thorchain to check whether the observer had been whitelist or not
func (b *ThorchainBridge) EnsureNodeWhitelisted() error {
	status, err := b.FetchNodeStatus()
	if err != nil {
		return fmt.Errorf("failed to get node status: %w", err)
	}
	if status == stypes.NodeStatus_Unknown {
		return fmt.Errorf("node account status %s , will not be able to forward transaction to thorchain", status)
	}
	return nil
}

// FetchNodeStatus get current node status from thorchain
func (b *ThorchainBridge) FetchNodeStatus() (stypes.NodeStatus, error) {
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return stypes.NodeStatus_Unknown, fmt.Errorf("fail to get address,err: %w", err)
	}
	bepAddr := addr.String()
	if len(bepAddr) == 0 {
		return stypes.NodeStatus_Unknown, errors.New("bep address is empty")
	}
	na, err := b.GetNodeAccount(bepAddr)
	if err != nil {
		return stypes.NodeStatus_Unknown, fmt.Errorf("failed to get node status: %w", err)
	}
	return na.Status, nil
}

// IsCatchingUp returns bool for if thorchain is catching up to the rest of the
// nodes. Returns yes, if it is, false if it is caught up.
func (b *ThorchainBridge) IsCatchingUp() (bool, error) {
	uri := url.URL{
		Scheme: "http",
		Host:   b.cfg.ChainRPC,
		Path:   StatusEndpoint,
	}
	httpClient := http.Client{
		Timeout: time.Second * 30,
	}
	resp, err := httpClient.Get(uri.String())
	if err != nil {
		return false, fmt.Errorf("failed to get status data: %w", err)
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("request: %s, response status code is: %s,not expected", uri.String(), resp.Status)
	}

	var r struct {
		Result struct {
			SyncInfo struct {
				CatchingUp bool `json:"catching_up"`
			} `json:"sync_info"`
		} `json:"result"`
	}
	dc := json.NewDecoder(resp.Body)
	if err := dc.Decode(&r); err != nil {
		return false, fmt.Errorf("failed to unmarshal tendermint status: %w", err)
	}

	return r.Result.SyncInfo.CatchingUp, nil
}

// WaitToCatchUp wait for thorchain to catch up
func (b *ThorchainBridge) WaitToCatchUp() error {
	for {
		yes, err := b.IsCatchingUp()
		if err != nil {
			return err
		}
		if !yes {
			break
		}
		b.logger.Info().Msg("thorchain is not caught up... waiting...")
		time.Sleep(constants.ThorchainBlockTime)
	}
	return nil
}

func (b *ThorchainBridge) getContextWithTimeout() (context.Context, context.CancelFunc) {
	// Let's default the timeout to 30s
	return context.WithTimeout(context.Background(), time.Second*30)
}

// GetAsgards retrieve all the asgard vaults from thorchain
func (b *ThorchainBridge) GetAsgards() (stypes.Vaults, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Asgards(ctx, &stypes.QueryAsgardsRequest{})
	if err != nil {
		return nil, fmt.Errorf("fail to get asgard vaults: %w", err)
	}

	var vaults stypes.Vaults
	for _, item := range resp.Vaults {
		vaults = append(vaults, stypes.Vault{
			BlockHeight:           item.BlockHeight,
			PubKey:                item.PubKey,
			Coins:                 item.Coins,
			Type:                  item.Type,
			Status:                item.Status,
			StatusSince:           item.StatusSince,
			Membership:            item.Membership,
			Chains:                item.Chains,
			InboundTxCount:        item.InboundTxCount,
			OutboundTxCount:       item.OutboundTxCount,
			PendingTxBlockHeights: item.PendingTxBlockHeights,
			Routers:               item.Routers,
		})
	}
	return vaults, nil
}

// GetPubKeys retrieve asgard vaults and yggdrasil vaults , and it's relevant smart contracts
func (b *ThorchainBridge) GetPubKeys() ([]PubKeyContractAddressPair, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.VaultPubkeys(ctx, &stypes.QueryVaultPubKeysRequest{})
	if err != nil {
		return nil, fmt.Errorf("fail to get asgard vaults: %w", err)
	}
	var addressPairs []PubKeyContractAddressPair
	for _, v := range resp.Asgards {
		kp := PubKeyContractAddressPair{
			PubKey:    common.PubKey(v.PubKey),
			Contracts: make(map[common.Chain]common.Address),
		}
		for _, item := range v.Routers {
			kp.Contracts[item.Chain] = item.Router
		}
		addressPairs = append(addressPairs, kp)
	}
	return addressPairs, nil
}

// PostNetworkFee send network fee message to THORNode
func (b *ThorchainBridge) PostNetworkFee(height int64, chain common.Chain, transactionSize, transactionRate uint64) (common.TxID, error) {
	nodeStatus, err := b.FetchNodeStatus()
	if err != nil {
		return common.BlankTxID, fmt.Errorf("failed to get node status: %w", err)
	}

	if nodeStatus != stypes.NodeStatus_Active {
		return common.BlankTxID, nil
	}
	start := time.Now()
	defer func() {
		b.m.GetHistograms(metrics.SignToThorchainDuration).Observe(time.Since(start).Seconds())
	}()
	addr, err := b.keys.GetSignerInfo().GetAddress()
	if err != nil {
		return common.BlankTxID, fmt.Errorf("failed to get address: %w", err)
	}
	msg := stypes.NewMsgNetworkFee(height, chain, transactionSize, transactionRate, addr)
	return b.Broadcast(msg)
}

// GetConstants from thornode
func (b *ThorchainBridge) GetConstants() (map[string]int64, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Constants(ctx, &stypes.QueryConstantsRequest{})
	if err != nil {
		return nil, fmt.Errorf("fail to get constants: %w", err)
	}

	return resp.Int_64Values, nil
}

// RagnarokInProgress is to query thorchain to check whether ragnarok had been triggered
func (b *ThorchainBridge) RagnarokInProgress() (bool, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Ragnarok(ctx, &stypes.QueryRagnarokRequest{})
	if err != nil {
		return false, fmt.Errorf("fail to get ragnarok status: %w", err)
	}
	return resp.RagnarokInProgress, nil
}

// GetThorchainVersion retrieve thorchain version
func (b *ThorchainBridge) GetThorchainVersion() (semver.Version, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Version(ctx, &stypes.QueryVersionRequest{})
	if err != nil {
		return semver.Version{}, fmt.Errorf("fail to get THORChain version: %w", err)
	}
	return semver.MustParse(resp.Current), nil
}

// GetMimir - get mimir settings
func (b *ThorchainBridge) GetMimir(key string) (int64, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.MimirWithKey(ctx, &stypes.QueryMimirWithKeyRequest{Name: key})
	if err != nil {
		return 0, fmt.Errorf("fail to get mimir(%s): %w", key, err)
	}
	return resp.Value, nil
}

// PubKeyContractAddressPair is an entry to map pubkey and contract addresses
type PubKeyContractAddressPair struct {
	PubKey    common.PubKey
	Contracts map[common.Chain]common.Address
}

// GetContractAddress retrieve the contract address from asgard
func (b *ThorchainBridge) GetContractAddress() ([]PubKeyContractAddressPair, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.InboundAddresses(ctx, &stypes.QueryInboundAddressesRequest{})
	if err != nil {
		return nil, fmt.Errorf("fail to get inbound addresses: %w", err)
	}
	var result []PubKeyContractAddressPair
	for _, item := range resp.InboundAddresses {
		pk, err := common.NewPubKey(item.Pubkey)
		if err != nil {
			b.logger.Err(err).Msgf("fail to parse pubkey(%s)", item.Pubkey)
			continue
		}
		c, err := common.NewChain(item.Chain)
		if err != nil {
			b.logger.Err(err).Msgf("fail to parse chain(%s)", item.Chain)
			continue
		}
		routerAddr, err := common.NewAddress(item.Router)
		if err != nil {
			b.logger.Err(err).Msgf("fail to parse router address(%s)", item.Router)
			continue
		}
		exist := false
		for _, pair := range result {
			if item.Pubkey == pair.PubKey.String() {
				pair.Contracts[c] = routerAddr
				exist = true
				break
			}
		}
		if !exist {
			pair := PubKeyContractAddressPair{
				PubKey:    pk,
				Contracts: map[common.Chain]common.Address{},
			}
			pair.Contracts[c] = routerAddr
			result = append(result, pair)
		}
	}
	return result, nil
}

// GetPools get pools from THORChain
func (b *ThorchainBridge) GetPools() (stypes.Pools, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Pools(ctx, &stypes.QueryPoolsRequest{})
	if err != nil {
		return nil, fmt.Errorf("fail to get pools addresses: %w", err)
	}
	var pools stypes.Pools
	for _, item := range resp.Pools {
		pools = append(pools, stypes.Pool{
			BalanceRune:         item.BalanceRune,
			BalanceAsset:        item.BalanceAsset,
			Asset:               item.Asset,
			LPUnits:             item.LpUnits,
			Status:              item.Status,
			Decimals:            item.Decimals,
			SynthUnits:          item.SynthUnits,
			PendingInboundRune:  item.PendingInboundRune,
			PendingInboundAsset: item.PendingInboundAsset,
		})
	}
	return pools, nil
}

// GetTHORName get THORName from THORChain
func (b *ThorchainBridge) GetTHORName(name string) (stypes.THORName, error) {
	ctx, cancel := b.getContextWithTimeout()
	defer cancel()
	resp, err := b.qc.Thorname(ctx, &stypes.QueryThornameRequest{Name: name})
	if err != nil {
		return stypes.THORName{}, fmt.Errorf("fail to get THORName: %w", err)
	}
	respName := stypes.THORName{
		Name:              resp.Name,
		ExpireBlockHeight: resp.ExpireBlockHeight,
	}
	if len(resp.PreferredAsset) > 0 {
		asset, err := common.NewAsset(resp.PreferredAsset)
		if err != nil {
			return stypes.THORName{}, fmt.Errorf("fail to parse preferred asset(%s),err: %w", resp.PreferredAsset, err)
		}
		respName.PreferredAsset = asset
	}
	addr, err := cosmos.AccAddressFromBech32(resp.Owner)
	if err != nil {
		return stypes.THORName{}, fmt.Errorf("fail to parse owner address (%s),err: %w", resp.Owner, err)
	}
	respName.Owner = addr
	for _, item := range resp.Aliases {
		respName.Aliases = append(respName.Aliases, stypes.THORNameAlias{
			Chain:   item.Chain,
			Address: item.Address,
		})
	}
	return respName, nil
}

// SetQueryClient expose this method so we
func (b *ThorchainBridge) SetQueryClient(client stypes.QueryClient) {
	b.qc = client
}
