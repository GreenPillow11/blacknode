package common

import (
	"encoding/base64"
	"os"

	"github.com/cosmos/cosmos-sdk/codec"

	"gitlab.com/blackprotocol/blacknode/common/cosmos"
)

// Sign an array of bytes.
// Returns (signature, pubkey, error)
func Sign(buf []byte, cdc codec.Codec) ([]byte, []byte, error) {
	kbs, err := cosmos.GetKeybase(os.Getenv(cosmos.EnvChainHome), cdc)
	if err != nil {
		return nil, nil, err
	}

	sig, pubkey, err := kbs.Keybase.Sign(kbs.SignerName, buf)
	if err != nil {
		return nil, nil, err
	}

	return sig, pubkey.Bytes(), nil
}

func SignBase64(buf []byte, cdc codec.Codec) (string, string, error) {
	sig, pubkey, err := Sign(buf, cdc)
	if err != nil {
		return "", "", err
	}

	return base64.StdEncoding.EncodeToString(sig),
		base64.StdEncoding.EncodeToString(pubkey), nil
}
