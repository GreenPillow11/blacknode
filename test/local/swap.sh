#!/bin/bash
# shellcheck disable=SC2059
# add a few single side liquidity
set -x
set -o pipefail
# TARGET_HOST should be the host has both thornode & mock binance running on different ports
TARGET_HOST=127.0.0.1
if [ -z "$1" ]; then
  echo "Missing mock binance host, assume it is localhost"
else
  TARGET_HOST="$1"
fi

for i in $(seq 1 1 2)
do
  SIGNER_PASSWD=password
  SIGNER_NAME="test$i"

  THOR_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show "$SIGNER_NAME" --keyring-backend=file --output json | jq -r '.address')
  # when the last command's error code is not zero , which usually means the name doesn't exist
  if [ $? -gt 0 ]; then
    printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode keys add "$SIGNER_NAME" --keyring-backend=file
    THOR_ADDRESS=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --output json | jq -r '.address')
  fi
  PUBKEY=$(printf "$SIGNER_PASSWD\n" | blacknode keys show $SIGNER_NAME --keyring-backend=file --pubkey | blacknode pubkey)
  BNB_ADDRESS=$(NET=testnet pubkey2address -p $PUBKEY | grep tbnb | awk '{ print $NF }')
  echo "$BNB_ADDRESS"
  POOL_ADDRESS=$(curl -s $TARGET_HOST:1317/thorchain/inbound_addresses | jq -r '.inbound_addresses | .[]|select(.chain=="BNB") .address')

  # swap BNB to RUNE
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"BNB\", \"amount\": 1000000000}
    ],
    \"memo\": \"swap:THOR.RUNE:$THOR_ADDRESS\"
  }]" "$TARGET_HOST":26660/broadcast/easy

  # send in LOK
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"LOK-3C0\", \"amount\": 40000000000}
    ],
    \"memo\": \"swap:THOR.RUNE:$THOR_ADDRESS\"
  }]" "$TARGET_HOST":26660/broadcast/easy

  # send in BTCB-101
  curl -vvv -s -X POST -d "[{
    \"from\": \"$BNB_ADDRESS\",
    \"to\": \"$POOL_ADDRESS\",
    \"coins\":[
        {\"denom\": \"BTCB-101\", \"amount\": 40000000000}
    ],
    \"memo\": \"swap:THOR.RUNE:$THOR_ADDRESS\"
  }]" "$TARGET_HOST":26660/broadcast/easy

  # swap RUNE to BNB
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 2000000000 rune swap:BNB.BNB:"$BNB_ADDRESS" --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from "$SIGNER_NAME" --keyring-backend=file --yes

  # swap RUNE to BNB.LOK-3C0
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 2000000000 rune swap:BNB.LOK-3C0:"$BNB_ADDRESS" --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from "$SIGNER_NAME" --keyring-backend=file --yes

  # swap RUNE to BNB.BTCB-101
  printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 2000000000 rune swap:BNB.BTCB-101:"$BNB_ADDRESS" --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from "$SIGNER_NAME" --keyring-backend=file --yes

  # swap synth
  curl -vvv -s -X POST -d "[{
      \"from\": \"$BNB_ADDRESS\",
      \"to\": \"$POOL_ADDRESS\",
      \"coins\":[
          {\"denom\": \"BNB\", \"amount\": 1000000000}
      ],
      \"memo\": \"swap:BNB/BNB:$THOR_ADDRESS\"
    }]" "$TARGET_HOST":26660/broadcast/easy
done
