#!/bin/bash
# shellcheck disable=SC2059
# add a few single side liquidity
set -x
set -o pipefail
# TARGET_HOST should be the host has both thornode & mock binance running on different ports
TARGET_HOST=127.0.0.1
if [ -z "$1" ]; then
  echo "Missing mock binance host, assume it is localhost"
else
  TARGET_HOST="$1"
fi

# withdraw test2's fund
SIGNER_PASSWD="password"
SIGNER_NAME=test2

printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 1 rune withdraw:BNB.BNB --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 10000000
printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 1 rune withdraw:BNB.LOK-3C0 --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 10000000
printf "$SIGNER_PASSWD\n$SIGNER_PASSWD\n" | blacknode tx thorchain deposit 1 rune withdraw:BNB.BTCB-101 --chain-id thorchain --node tcp://"$TARGET_HOST":26657 --from $SIGNER_NAME --keyring-backend=file --yes --gas 10000000