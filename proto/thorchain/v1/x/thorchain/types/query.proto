syntax = "proto3";
package types;

import "gogoproto/gogo.proto";
import "google/api/annotations.proto";
import "cosmos/base/v1beta1/coin.proto";
import "cosmos_proto/cosmos.proto";
import "thorchain/v1/x/thorchain/types/type_liquidity_provider.proto";
import "thorchain/v1/x/thorchain/types/type_observed_tx.proto";
import "thorchain/v1/common/common.proto";
import "thorchain/v1/x/thorchain/types/type_tss_metric.proto";
import "thorchain/v1/x/thorchain/types/type_tx_out.proto";
import "thorchain/v1/x/thorchain/types/type_keygen.proto";
import "thorchain/v1/x/thorchain/types/type_node_account.proto";
import "thorchain/v1/x/thorchain/types/type_jail.proto";
import "thorchain/v1/x/thorchain/types/type_vault.proto";
import "thorchain/v1/x/thorchain/types/type_chain_contract.proto";
import "thorchain/v1/x/thorchain/types/type_mimir.proto";
import "thorchain/v1/x/thorchain/types/type_ban_voter.proto";
import "thorchain/v1/x/thorchain/types/type_pool.proto";
import "thorchain/v1/x/thorchain/types/claim.proto";

option go_package = "gitlab.com/blackprotocol/blacknode/x/thorchain/types";

service Query {
  rpc InboundAddresses(QueryInboundAddressesRequest)
    returns (QueryInboundAddressesResponse) {
    option (google.api.http).get = "/thorchain/inbound_addresses";
  };
  rpc Pools(QueryPoolsRequest) returns (QueryPoolsResponse) {
    option (google.api.http).get = "/thorchain/pools";
  }
  rpc Pool(QueryPoolRequest) returns (QueryPoolResponse) {
    option (google.api.http).get = "/thorchain/pool/{asset}";
  }
  rpc LiquidityProviders(QueryLiquidityProvidersRequest)
    returns (QueryLiquidityProvidersResponse) {
    option (google.api.http).get = "/thorchain/liquidity_providers/{asset}";
  }
  rpc LiquidityProvider(QueryLiquidityProviderRequest)
    returns (QueryLiquidityProviderResponse) {
    option (google.api.http).get =
      "/thorchain/liquidity_providers/{asset}/{address}";
  }
  rpc Saver(QuerySaverRequest) returns (QuerySaverResponse) {
    option (google.api.http).get = "/thorchain/saver/{asset}/{address}";
  }
  rpc Savers(QuerySaversRequest) returns (QuerySaversResponse) {
    option (google.api.http).get = "/thorchain/savers/{asset}";
  }
  rpc TxVoters(QueryTxVoterRequest) returns (QueryTxVoterResponse) {
    option (google.api.http).get = "/thorchain/tx/{tx_id}/signers";
  }
  rpc Tx(QueryTxRequest) returns (QueryTxResponse) {
    option (google.api.http).get = "/thorchain/tx/{tx_id}";
  }
  rpc Keysigns(QueryKeysignsRequest) returns (QueryKeysignsResponse) {
    option (google.api.http).get = "/thorchain/keysign/{height}";
  }
  rpc KeysignsByPubKey(QueryKeysignsByPubKeyRequest)
    returns (QueryKeysignsByPubKeyResponse) {
    option (google.api.http).get = "/thorchain/keysign/{height}/{pub_key}";
  }
  rpc Keygens(QueryKeygensRequest) returns (QueryKeygensResponse) {
    option (google.api.http).get = "/thorchain/keygen/{height}";
  }
  rpc KeygensByPubKey(QueryKeygenByPubKeyRequest) returns (QueryKeygenByPubKeyResponse) {
    option (google.api.http).get = "/thorchain/keygen/{height}/{pub_key}";
  }
  rpc Queue(QueryQueueRequest) returns (QueryQueueResponse) {
    option (google.api.http).get = "/thorchain/queue";
  }
  rpc Outbound(QueryOutboundRequest) returns (QueryOutboundResponse) {
    option (google.api.http).get = "/thorchain/queue/outbound";
  }
  rpc ScheduledOutbound(QueryScheduledOutboundRequest)
    returns (QueryScheduledOutboundResponse) {
    option (google.api.http).get = "/thorchain/queue/scheduled";
  }
  rpc Heights(QueryHeightsRequest) returns (QueryHeightsResponse) {
    option (google.api.http).get = "/thorchain/lastblock";
  }
  rpc ChainHeights(QueryChainHeightsRequest)
    returns (QueryChainHeightsResponse) {
    option (google.api.http).get = "/thorchain/lastblock/{chain}";
  }
  rpc Nodes(QueryNodesRequest) returns (QueryNodesResponse) {
    option (google.api.http).get = "/thorchain/nodes";
  }
  rpc Node(QueryNodeRequest) returns (QueryNodeResponse) {
    option (google.api.http).get = "/thorchain/node/{address}";
  }
  rpc Network(QueryNetworkRequest) returns (QueryNetworkResponse) {
    option (google.api.http).get = "/thorchain/network";
  }
  rpc POL(QueryPOLRequest) returns (QueryPOLResponse) {
    option (google.api.http).get = "/thorchain/pol";
  }
  rpc ModuleBalance(QueryModuleBalanceRequest)
    returns (QueryModuleBalanceResponse) {
    option (google.api.http).get = "/thorchain/balance/module/{name}";
  }
  rpc Asgards(QueryAsgardsRequest) returns (QueryAsgardsResponse) {
    option (google.api.http).get = "/thorchain/vaults/asgard";
  }
  rpc Vault(QueryVaultRequest) returns (QueryVaultResponse) {
    option (google.api.http).get = "/thorchain/vault/{pub_key}";
  }
  rpc VaultPubkeys(QueryVaultPubKeysRequest)
    returns (QueryVaultPubKeysResponse) {
    option (google.api.http).get = "/thorchain/vaults/pubkeys";
  }
  rpc Constants(QueryConstantsRequest) returns (QueryConstantsResponse) {
    option (google.api.http).get = "/thorchain/constants";
  }
  rpc Mimirs(QueryMimirsRequest) returns (QueryMimirsResponse) {
    option (google.api.http).get = "/thorchain/mimir";
  }
  rpc MimirWithKey(QueryMimirWithKeyRequest)
    returns (QueryMimirWithKeyResponse) {
    option (google.api.http).get = "/thorchain/mimir/key/{name}";
  }
  rpc AdminMimirs(QueryAdminMimirRequest) returns (QueryAdminMimirResponse) {
    option (google.api.http).get = "/thorchain/mimir/admin";
  }
  rpc NodesMimir(QueryNodeMimirRequest) returns (QueryNodeMimirResponse) {
    option (google.api.http).get = "/thorchain/mimir/nodes";
  }
  rpc NodeMimirWithAddress(QueryNodeMimirWithAddressRequest)
    returns (QueryNodeMimirWithAddressResponse) {
    option (google.api.http).get = "/thorchain/mimir/node/{address}";
  }
  rpc AllNodeMimirs(QueryAllNodesMimirsRequest)
    returns (QueryAllNodesMimirsResponse) {
    option (google.api.http).get = "/thorchain/mimir/node_all";
  }
  rpc Ban(QueryBanRequest) returns (QueryBanResponse) {
    option (google.api.http).get = "/thorchain/ban/{address}";
  }
  rpc Ragnarok(QueryRagnarokRequest) returns (QueryRagnarokResponse) {
    option (google.api.http).get = "/thorchain/ragnarok";
  }
  rpc KeygenMetric(QueryKeygenMetricRequest)
    returns (QueryKeygenMetricResponse) {
    option (google.api.http).get = "/thorchain/metric/keygen/{pub_key}";
  }
  rpc TssMetric(QueryTssMetricRequest) returns (QueryTssMetricResponse) {
    option (google.api.http).get = "/thorchain/metrics";
  }
  rpc Thorname(QueryThornameRequest) returns (QueryThornameResponse) {
    option (google.api.http).get = "/thorchain/thorname/{name}";
  }
  rpc Version(QueryVersionRequest) returns(QueryVersionResponse){
    option (google.api.http).get = "/thorchain/version";
  }
  rpc Claims(QueryClaimsRequest) returns(QueryClaimsResponse) {
    option (google.api.http).get = "/thorchain/claims";
  }
  rpc ClaimsByHeight(QueryClaimsByHeightRequest) returns(QueryClaimsByHeightResponse){
    option (google.api.http).get = "/thorchain/claim-by-height";
  }
  // TODO add those few quote RPC
}

// Claims
message QueryClaimsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}
message QueryClaimsResponse {
  repeated ClaimItem claims = 1;
}

message QueryClaimsByHeightRequest{
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  int64 height = 1;
}

message QueryClaimsByHeightResponse{
  BlockClaims blockclaims = 1;
  string signature = 2;
}
// version
message QueryVersionRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryVersionResponse{
  string current = 1;
  string next = 2;
  string querier = 3;
}
// thorname
message QueryThornameRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string name = 1;
}

message QueryThornameAlias {
  string chain = 1 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Chain"];
  string address = 2 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Address"];
}

message QueryThornameResponse {
  string name = 1;
  int64 expire_block_height = 2;
  string owner = 3;
  string preferred_asset = 4;
  repeated QueryThornameAlias aliases = 5;
}

// tss metrics
message QueryTssMetricRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryTssMetricResponse {
  repeated TssKeygenMetric keygen = 1;
  TssKeysignMetric keysign = 2;
}

message QueryKeygenMetricRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string pub_key = 1 ;
}

message QueryKeygenMetricResponse {
  repeated TssKeygenMetric metrics = 1;
}

// ragnarok
message QueryRagnarokRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryRagnarokResponse {
  bool ragnarok_in_progress = 1;
}
// ban
message QueryBanRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string address = 1 [(cosmos_proto.scalar) = "cosmos.AddressString"];
}

message QueryBanResponse {
  BanVoter ban_voter = 1;
}
// mimir
message QueryAllNodesMimirsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryAllNodesMimirsResponse {
  repeated NodeMimir mimirs = 1 [(gogoproto.nullable) = false];
}

message QueryNodeMimirWithAddressRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string address = 1 [(cosmos_proto.scalar) = "cosmos.AddressString"];
}

message QueryNodeMimirWithAddressResponse {
  map<string, int64> mimirs = 1;
}

message QueryNodeMimirRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryNodeMimirResponse {
  map<string, int64> mimirs = 1;
}

message QueryAdminMimirRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryAdminMimirResponse {
  map<string, int64> mimirs = 1;
}

message QueryMimirWithKeyRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string name = 1;
}

message QueryMimirWithKeyResponse {
  int64 value = 1;
}
message QueryMimirsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryMimirsResponse {
  map<string, int64> mimirs = 1;
}
// miscellaneous
message QueryConstantsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryConstantsResponse {
  map<string, int64> int_64_values = 1;
  map<string, bool> bool_values = 2;
  map<string, string> string_values = 3;
}
// vaults
message QueryVaultPubKeysRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryVaultPubKeyContract {
  string pub_key = 1 ;
  repeated ChainContract routers = 2 [(gogoproto.nullable) = false];
}

message QueryVaultPubKeysResponse {
  repeated QueryVaultPubKeyContract asgards = 1;
}

message QueryVaultRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string pub_key = 1 ;
}

message QueryVaultResponse {
  QueryVault vault = 1;
}

message QueryAsgardsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryChainAddress {
  string chain = 1 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Chain"];
  string address = 2 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Address"];
}

message QueryVault {
  int64 block_height = 1;
  string pub_key = 2 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.PubKey"];
  repeated common.Coin coins = 3 [
    (gogoproto.castrepeated) =
      "gitlab.com/blackprotocol/blacknode/common.Coins",
    (gogoproto.nullable) = false
  ];
  VaultType type = 4;
  VaultStatus status = 5;
  int64 status_since = 6;
  repeated string membership = 7;
  repeated string chains = 8;
  int64 inbound_tx_count = 9;
  int64 outbound_tx_count = 10;
  repeated int64 pending_tx_block_heights = 11;
  repeated types.ChainContract routers = 12 [(gogoproto.nullable) = false];
  repeated QueryChainAddress addresses = 13;
}

message QueryAsgardsResponse {
  repeated QueryVault vaults = 1;
}
// module balance
message QueryModuleBalanceRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  string name = 1;
}

message QueryModuleBalanceResponse {
  string name = 1;
  string address = 2 [(cosmos_proto.scalar) = "cosmos.AddressString"];
  repeated cosmos.base.v1beta1.Coin coins = 3 [(gogoproto.nullable) = false, (gogoproto.castrepeated) = "github.com/cosmos/cosmos-sdk/types.Coins"];
}
// pol
message QueryPOLRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryPOLResponse {
  string rune_deposited = 1;
  string rune_withdrawn = 2;
  string value = 3;
  string pnl = 4;
  string current_deposit = 5;
}
// network
message QueryNetworkRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryNetworkResponse {
  string bond_reward_rune = 1;
  string total_bond_units = 2;
  string total_reserve = 3;
  string burned_bep2_rune = 4;
  string burned_erc20_rune = 5;
}
// nodes
message QueryNodesRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryNodeAccountPreflightCheck {
  NodeStatus status = 1;
  string description = 2;
  int64 code = 3;
}

message QueryChainHeight {
  string chain = 1 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Chain"];
  int64 height = 2;
}

message QueryNodeAccount {
  string node_address = 1 [(cosmos_proto.scalar) = "cosmos.AddressString"];
  NodeStatus status = 2;
  common.PubKeySet pub_key_set = 3;
  string validator_cons_pub_key = 4;
  string bond = 5 [
    (gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint",
    (gogoproto.nullable) = false
  ];
  int64 active_block_height = 6;
  string bond_address = 7
  [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Address"];
  int64 status_since = 8;
  repeated string signer_membership = 9;
  bool requested_to_leave = 10;
  bool forced_to_leave = 11;
  uint64 leave_score = 12;
  string ip_address = 13 [(gogoproto.customname) = "IPAddress"];
  string version = 14;
  int64 slash_points = 15;
  Jail jail = 16;
  string current_award = 17 [
    (gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint",
    (gogoproto.nullable) = false
  ];
  repeated QueryChainHeight observe_chains = 18;
  QueryNodeAccountPreflightCheck preflight_status = 19;
  BondProviders bond_providers = 20;
}

message QueryNodesResponse {
  repeated QueryNodeAccount nodes = 1;
}

message QueryNodeRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string address = 1 [(cosmos_proto.scalar) = "cosmos.AddressString"];
}

message QueryNodeResponse {
  QueryNodeAccount node = 1;
}
// heights
message QueryHeightsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}
message QueryLastBlockHeight {
  string chain = 1 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Chain"];
  int64 last_observed_in = 2;
  int64 last_signed_out = 3;
  int64 thorchain = 4;
}
message QueryHeightsResponse {
  repeated QueryLastBlockHeight block_heights = 1;
}
message QueryChainHeightsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string chain = 1 ;
}
message QueryChainHeightsResponse {
  QueryLastBlockHeight block_height = 1;
}
// Queue
message QueryOutboundRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message QueryOutboundResponse {
  repeated TxOutItem tx_out_items = 1;
}

message QueryScheduledOutboundRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}
message QueryTxOutItem {
  string chain = 1 [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Chain"];
  string to_address = 2
  [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.Address"];
  string vault_pub_key = 3
  [(gogoproto.casttype) =
    "gitlab.com/blackprotocol/blacknode/common.PubKey"];
  common.Coin coin = 4 [(gogoproto.nullable) = false];
  string memo = 5;
  repeated common.Coin max_gas = 6 [
    (gogoproto.castrepeated) = "gitlab.com/blackprotocol/blacknode/common.Gas",
    (gogoproto.nullable) = false
  ];
  int64 gas_rate = 7;
  string in_hash = 8
  [(gogoproto.casttype) = "gitlab.com/blackprotocol/blacknode/common.TxID"];
  string out_hash = 9 [(gogoproto.casttype) = "gitlab.com/blackprotocol/blacknode/common.TxID"];
  string module_name = 10 [json_name="-", (gogoproto.jsontag) = "-"];
  string aggregator = 11;
  string aggregator_target_asset = 12;
  string aggregator_target_limit = 13 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = true];
  int64 height = 14;
}

message QueryScheduledOutboundResponse {
  repeated QueryTxOutItem tx_out_items = 1;
}
message QueryQueueRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}
message QueryQueueResponse {
  int64 swap = 1;
  int64 outbound = 2;
  int64 internal = 3;
  string scheduled_outbound_value = 4 [
    (gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint",
    (gogoproto.nullable) = false
  ];
}
// keygen
message QueryKeygensRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  int64 height = 1;
}

message QueryKeygensResponse {
  KeygenBlock keygen_block = 1;
  string signature = 2;
}

message QueryKeygenByPubKeyRequest{
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  int64 height = 1;
  string pub_key = 2 ;
}

message QueryKeygenByPubKeyResponse {
  KeygenBlock keygen_block = 1;
  string signature = 2;
}
// Keysigns
message QueryKeysignsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  int64 height = 1;
}
message QueryKeysignsResponse {
  TxOut keysign = 1;
  string signature = 2;
}
message QueryKeysignsByPubKeyRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  int64 height = 1;
  string pub_key = 2;
}
message QueryKeysignsByPubKeyResponse {
  TxOut keysign = 1;
  string signature = 2;
}
// Tx or TxVoter
message QueryTxVoterRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;

  string tx_id = 1;
}

message QueryTxVoterResponse {
  ObservedTxVoter tx_voter = 1;
}

message QueryTxRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string tx_id = 1;
}

message QueryTxResponse {
  ObservedTx observed_tx = 1;
  TssKeysignMetric keysign_metric = 2;
}

// Savers & Liquidity Providers
message QuerySaverRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string asset = 1 [(cosmos_proto.scalar) = "cosmos.Asset"];
  string address = 2 [(cosmos_proto.scalar) = "cosmos.AddressString"];
}

message QuerySaversRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string asset = 1 [(cosmos_proto.scalar) = "cosmos.Asset"];
}

message QuerySaversResponse {
  repeated LiquidityProvider savers = 1;
}
message QuerySaverResponse {
  LiquidityProvider saver = 1;
}
message QueryLiquidityProvidersRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string asset = 1 [(cosmos_proto.scalar) = "cosmos.Asset"];
}

message QueryLiquidityProvidersResponse {
  repeated LiquidityProvider liquidity_providers = 1;
}

message QueryLiquidityProviderRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string asset = 1 [(cosmos_proto.scalar) = "cosmos.Asset"];
  string address = 2 [(cosmos_proto.scalar) = "cosmos.AddressString"];
}

message QueryLiquidityProviderResponse {
  LiquidityProvider liquidity_provider = 1;
}
// QueryPool and QueryPools
message QueryPoolRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
  string asset = 1 [(cosmos_proto.scalar) = "cosmos.AddressString"];
  ;
}
message QueryPoolResponse {
  QueryPool pool = 1;
}

message QueryPool {
  string balance_rune = 1 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string balance_asset = 2 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  common.Asset asset = 3 [(gogoproto.nullable) = false];
  string lp_units = 4 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string pool_units = 5 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  PoolStatus status = 6 ;
  int64 decimals = 7;
  string synth_units = 8 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string synth_supply = 9 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string pending_inbound_rune = 10 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string pending_inbound_asset = 11 [(gogoproto.customtype) = "github.com/cosmos/cosmos-sdk/types.Uint", (gogoproto.nullable) = false];
  string savers_depth = 12;
  string savers_units = 13;
  bool synth_mint_paused = 14;
}
message QueryPoolsRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}
message QueryPoolsResponse {
  repeated QueryPool pools = 1;
}
message QueryInboundAddressesRequest {
  option (gogoproto.equal) = false;
  option (gogoproto.goproto_getters) = false;
}

message InboundAddress {
  string chain = 1;
  string pubkey = 2;
  string address = 3;
  string router = 4;
  bool halted = 5;
  bool global_trading_paused = 6;
  bool chain_trading_paused = 7;
  bool chain_lp_actions_paused = 8;
  string gas_rate = 9;
  string gas_rate_units = 10;
  string outbound_tx_size = 11;
  string outbound_fee = 12;
  string dust_threshold = 13;
}

message QueryInboundAddressesResponse {
  repeated InboundAddress inbound_addresses = 1;
}